package com.epam.training.controllers.utils;

import com.epam.training.model.entity.BaseEntity;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.BaseService;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.training.controllers.utils.Constants.PAGE_KEY;

/**
 * Utility class for common methods for several controllers
 */

public class Utils {

    private Utils() {
    }

    public static int getPageNumber(Integer page) {
        int pageNumber = 0;
        if (page != null) {
            // "- 1" because in PagedListHolder page count starts from 0
            pageNumber = page - 1;
            if (pageNumber < 0){
                pageNumber = 0;
            }
        }
        return pageNumber;
    }

    public static <O extends BaseEntity> int getPageNumber(BaseService baseService, Long id) {
        int pageNumber = 0;
        Page<O> objects = baseService.getAll(pageNumber);
        int allPageCount = objects.getTotalPages();
        for (int i = 1; i <= allPageCount; i++) {
            if (objects
                    .getContent()
                    .stream()
                    .anyMatch(p -> p.getId().equals(id))) {
                pageNumber = i;
                return pageNumber;
            }
            objects = baseService.getAll(i);
        }
        return pageNumber;
    }

    public static <O extends BaseEntity> int getLastPageNumber(BaseService baseService) {
        int pageNumber = 1;
        Page<O> objects = baseService.getAll(pageNumber);
        pageNumber = objects.getTotalPages();
        objects = baseService.getAll(pageNumber - 1);
        if (objects.getContent().size() == 10) {
            return pageNumber + 1;
        }
        return pageNumber;
    }




    public static List<String> getCategoryNameHierarchy(CategoryH2ServiceImpl categoryService, String regex, Long withoutId) {
        List<Category> categories = categoryService.getAll();
        List<String> categoryNameHierarchy = new ArrayList<>();
        List<Category> superCategories = categories
                .stream()
                .filter(c -> c.getSuperCategory() == null)
                .collect(Collectors.toList());
        for (Category category : superCategories) {
            StringBuilder hierarchySplit = new StringBuilder();
            if (!category.getId().equals(withoutId)) {
                categoryNameHierarchy.add(category.getName() + " id(" + category.getId() + ")");
                if (!category.getSubCategories().isEmpty()) {
                    addSubCategoryName(categoryNameHierarchy, category, hierarchySplit, regex, withoutId);
                }
            }
        }
        return categoryNameHierarchy;
    }

    private static void addSubCategoryName(List<String> categoryName, Category superCategory
            , StringBuilder hierarchySplit, String regex, Long withoutId) {
        StringBuilder split = new StringBuilder(hierarchySplit.toString());
        split.append(regex);
        for (Category category : superCategory.getSubCategories()) {
            if (!category.getId().equals(withoutId)) {
                String resultName = split.toString() + category.getName() + " id(" + category.getId() + ")";
                categoryName.add(resultName);
                if (!category.getSubCategories().isEmpty()) {
                    addSubCategoryName(categoryName, category, split, regex, withoutId);
                }
            }
        }
    }

    public static Long getClearId(String itemsName) {
        int startIndexId = itemsName.lastIndexOf('(') + 1;
        int endIndexId = itemsName.lastIndexOf(')');
        String clearId = itemsName.substring(startIndexId, endIndexId).trim();
        Long id;
        try {
            id = Long.parseLong(clearId);
        } catch (NumberFormatException e) {
            return 0L;
        }
        return id;
    }

    public static ModelAndView deleteAndGetView(BaseService baseService, Long id, int pageNumber, String defUrl){
        boolean result = baseService.delete(id);
        if (!result) {
            ModelAndView mv = new ModelAndView(defUrl + PAGE_KEY + pageNumber);
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        return new ModelAndView("redirect:" + defUrl + PAGE_KEY + pageNumber);
    }

    public static List<String> getProductNameList(ProductH2ServiceImpl productService) {
        List<String> productNameList = new ArrayList<>();
        List<Product> products = productService.getAll();
        for (Product product : products) {
            productNameList.add(product.getName() + " (" + product.getId() + ")");
        }
        return productNameList;
    }

}