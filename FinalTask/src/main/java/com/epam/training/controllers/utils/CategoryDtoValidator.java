package com.epam.training.controllers.utils;

import com.epam.training.model.entity.dto.CategoryDto;

/**
 * class for validating form data in the form of a category
 */
public class CategoryDtoValidator {
    private CategoryDtoValidator() {
    }

    public static boolean isValidate(CategoryDto categoryDto) {
        return (!"".equals(categoryDto.getName()));
    }
}
