package com.epam.training.controllers;

import com.epam.training.controllers.utils.Constants;
import com.epam.training.controllers.utils.ProductDtoValidator;
import com.epam.training.controllers.utils.Utils;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Product;
import com.epam.training.model.entity.dto.ProductDto;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.PriceSoapServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import com.epam.training.service.Price;
import com.epam.training.service.PricePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.epam.training.controllers.utils.Constants.*;

/**
 * Controller for products
 * default url = "/v1/product";
 */
@RestController
@RequestMapping(value = "/" + Constants.VERSION_1_URL + "/" + Constants.PRODUCT_URL
        , produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProductController {

    private static final String DEF_URL = "/" + Constants.VERSION_1_URL + "/" + Constants.PRODUCT_URL;
    private static final String PRODUCT_PAGE_KEY = "productsPage";
    private static final String PRODUCT_DTO_KEY = "productDtoForm";
    private static final String PRODUCT_SEARCH_KEY = "product/product-search";
    private static final String CURRENCY_KEY = "currency";
    private static final String SEARCH_PARAM_KEY = "searchParam";

    private ProductH2ServiceImpl productService;
    private CategoryH2ServiceImpl categoryService;
    private PriceSoapServiceImpl priceSoapService;

    @Autowired
    public ProductController(ProductH2ServiceImpl productH2Service,
                             CategoryH2ServiceImpl categoryService,
                             PriceSoapServiceImpl priceSoapService
    ) {
        this.productService = productH2Service;
        this.categoryService = categoryService;
        this.priceSoapService = priceSoapService;
    }

    @GetMapping(value = {"", "/page/{page}"}, produces = "application/json;charset=UTF-8")
    public ModelAndView getProducts(@PathVariable(required = false, name = "page") Integer page) {
        int pageNumber = Utils.getPageNumber(page);
        ModelAndView mv = getProductsView();
        Page<Product> products = productService.getAll(pageNumber);
        mv.addObject(PRODUCT_PAGE_KEY, products);
        mv.addObject(PAGE_URL_KEY, DEF_URL + PAGE_KEY);
        return mv;
    }

    @GetMapping(value = {"/{id}"})
    public ModelAndView getProduct(
            @PathVariable("id") Long id
            , @RequestParam(required = false, name = "msg") String msg
    ) {
        Product product = productService.getById(id);
        ModelAndView mv = new ModelAndView("product/product", "product", product);
        if (product == null) {
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        mv.addObject(PRODUCT_DTO_KEY, new ProductDto());
        mv.addObject("categoryName",
                Utils.getCategoryNameHierarchy(categoryService, "===", 0L));
        mv.addObject("msg", msg);
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    @GetMapping(value = "/search")
    public ModelAndView getSearch() {
        ModelAndView mv = new ModelAndView(PRODUCT_SEARCH_KEY);
        mv.addObject(CURRENCY_KEY , Currency.values());
        return mv;
    }

    @GetMapping(value = "/search/byId")
    public ModelAndView getSearchById(@RequestParam(required = false) String productId) {
        ModelAndView mv = new ModelAndView();
        long idLong = 0;
        try {
            idLong = Long.parseLong(productId);
        } catch (NumberFormatException e) {
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in some field!");
            mv.setViewName(PRODUCT_SEARCH_KEY);
            return mv;
        }
        Product productSearch = productService.getById(idLong);
        return getSearchView(productSearch);
    }

    @GetMapping(value = {"/search/byName", "/search/byName/{page}"})
    public ModelAndView getSearchByName(
            @RequestParam(required = false) String name
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        int pageNumber = Utils.getPageNumber(page);
        Page<Product> products = productService.getByName(name, pageNumber);
        ModelAndView mv = getSearchView(products);
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byName/");
        mv.addObject(SEARCH_PARAM_KEY,
                "?name=" + name);
        return mv;
    }

    @GetMapping(value = {"/search/byPrice", "/search/byPrice/{page}"})
    public ModelAndView getSearchByPrice(
            @RequestParam(required = false) String priceValue
            , @RequestParam(required = false) String priceCurrency
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        ModelAndView mv = new ModelAndView();
        mv.addObject(CURRENCY_KEY , Currency.values());
        int price = 0;
        int pageNumber = Utils.getPageNumber(page);
        if (priceValue != null) {
            try {
                price = Integer.parseInt(priceValue);
            } catch (NumberFormatException e) {
                mv.setStatus(HttpStatus.BAD_REQUEST);
                mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in price value field!");
                mv.setViewName(PRODUCT_SEARCH_KEY);
                return mv;
            }
        } else {
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Price value cannot be empty");
            mv.setViewName(PRODUCT_SEARCH_KEY);
            return mv;
        }
        if (priceCurrency == null || priceCurrency.length() > 3) {
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in currency  field!");
            mv.setViewName(PRODUCT_SEARCH_KEY);
            return mv;
        }
        Currency currency = Currency.valueOf(priceCurrency);
        PricePage pricesPage = priceSoapService.getByCurrencyValue(currency,price,pageNumber-1);
        Set<Long> productsId = new HashSet<>();
        for (Price localPrice : pricesPage.getContent()) {
            productsId.add(localPrice.getProductId());
        }
        List<Product> productsList = new ArrayList<>();
        for (Long productId : productsId) {
            productsList.add(productService.getById(productId));
        }
        mv.addObject(CURRENCY_KEY , Currency.values());
        if (pricesPage.getNumberOfElements() < 1) {
            return getViewNotFound(mv);
        }
        mv = getProductsView();
        mv.addObject("pricesPage", pricesPage);
        mv.addObject("productsList", productsList);
        mv.addObject(  "pageNumber", pricesPage.getPageNumber());
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byPrice/");
        mv.addObject(SEARCH_PARAM_KEY,
                "?priceCurrency=" + priceCurrency + "&priceValue=" + priceValue);
        return mv;
    }

    @GetMapping(value = {"/search/byCategoryId", "/search/byCategoryId/{page}"})
    public ModelAndView getSearchByCategoryId(
            @RequestParam(required = false, name = "categoryId") String categoryId
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        int pageNumber = Utils.getPageNumber(page);
        ModelAndView mv = new ModelAndView();
        mv.addObject(CURRENCY_KEY , Currency.values());
        long idCategory = 0;
        try {
            idCategory = Long.parseLong(categoryId);
        } catch (NumberFormatException e) {
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.setViewName(PRODUCT_SEARCH_KEY);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in Category Id field!");
            return mv;
        }
        Page<Product> products = productService.getByCategoryId(idCategory, pageNumber);
        if (products.getNumberOfElements() < 1) {
            return getViewNotFound(mv);
        }
        mv = getProductsView();
        mv.addObject(PRODUCT_PAGE_KEY, products);
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byCategoryId/");
        mv.addObject(SEARCH_PARAM_KEY, "?categoryId=" + idCategory);
        return mv;
    }

    @PostMapping(value = {"/0", "/{id}"}
            , consumes = "application/json;charset=UTF-8"
            , produces = "application/json; charset=UTF-8"
    )
    public ModelAndView addNewProduct(@RequestBody ProductDto productDtoForm) {
        if (!ProductDtoValidator.isValidate(productDtoForm)) {
            ModelAndView mv = new ModelAndView("product/products");
            mv.addObject(PRODUCT_DTO_KEY, new ProductDto());
            mv.setStatus(HttpStatus.BAD_REQUEST);
            return mv;
        }
        Product product = productDtoForm.getProduct(categoryService);
        ModelAndView mv = new ModelAndView("redirect:" + DEF_URL
                + PAGE_KEY + Utils.getLastPageNumber(productService));
        productService.addProduct(product);
        return mv;
    }

    @PutMapping(value = "/{id}")
    public ModelAndView updateProduct(
            @PathVariable Long id,
            @ModelAttribute(PRODUCT_DTO_KEY) ProductDto productDto
            , final RedirectAttributes redirectAttributes
    ) {
        if (!ProductDtoValidator.isValidate(productDto)) {
            String errorMessage = "Incorrect data entered in some field!";
            return new ModelAndView("redirect:" + DEF_URL + "/" + id + "?msg=" + errorMessage);
        }
        ModelAndView mv = new ModelAndView();
        Product product = productDto.getProduct(categoryService);
        product.setId(id);
        productService.editProduct(product);
        int page = Utils.getPageNumber(productService, id);
        mv.setViewName("redirect:" + DEF_URL + PAGE_KEY + page);
        redirectAttributes.addAttribute(ERROR_MESSAGE_KEY, "Product update successfully!");
        return mv;
    }

    @DeleteMapping(value = "/{id}")
    public ModelAndView deleteProduct(@PathVariable Long id) {
        int pageNumber = Utils.getPageNumber(productService, id - 1);
        return Utils.deleteAndGetView(productService, id, pageNumber, DEF_URL);
    }

    private ModelAndView getSearchView(Product productSearch) {
        ModelAndView mv = new ModelAndView();
        mv.addObject(CURRENCY_KEY , Currency.values());
        List<Product> productsList = new ArrayList<>();
        if (productSearch == null) {
            return getViewNotFound(mv);
        }
        mv = getProductsView();
        productsList.add(productSearch);
        mv.addObject("productsList", productsList);
        return mv;
    }

    private ModelAndView getSearchView(Page<Product> products) {
        ModelAndView mv = new ModelAndView();
        mv.addObject(CURRENCY_KEY , Currency.values());
        if (products.getNumberOfElements() < 1) {
            return getViewNotFound(mv);
        }
        mv = getProductsView();
        mv.addObject(PRODUCT_PAGE_KEY, products);
        return mv;
    }

    private ModelAndView getProductsView() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("product/products");
        mv.addObject("categoryName",
                Utils.getCategoryNameHierarchy(categoryService, "===", 0L));
        mv.addObject(PRODUCT_DTO_KEY, new ProductDto());
        return mv;
    }

    private ModelAndView getViewNotFound( ModelAndView mv) {
        mv.setViewName(PRODUCT_SEARCH_KEY);
        mv.setStatus(HttpStatus.NOT_FOUND);
        mv.addObject(ERROR_MESSAGE_KEY, MSG_NOT_FOUND);
        return mv;
    }
}
