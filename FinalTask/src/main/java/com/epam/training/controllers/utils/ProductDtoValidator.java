package com.epam.training.controllers.utils;

import com.epam.training.model.entity.dto.ProductDto;

/**
 * class for validating form data in the form of a product
 */
public class ProductDtoValidator {
    private ProductDtoValidator() {
        //for sonar
    }

    public static boolean isValidate(ProductDto productDto) {
        if (productDto.getName().isEmpty() || productDto.getName() == null) {
            return false;
        }
        if (productDto.getCategories().isEmpty()) {
            return false;
        }
        String byn = productDto.getPriceBYN();
        String uan = productDto.getPriceUAN();
        String usd = productDto.getPriceUSD();
        String eur = productDto.getPriceEUR();
        try {
            int priceByn = byn.isEmpty() ? 0 : Integer.parseInt(byn);
            int priceUan = uan.isEmpty() ? 0 : Integer.parseInt(uan);
            int priceUsd = usd.isEmpty() ? 0 : Integer.parseInt(usd);
            int priceEur = eur.isEmpty() ? 0 : Integer.parseInt(eur);
            if (priceByn >= 0 &&
                    priceUan >= 0 &&
                    priceUsd >= 0 &&
                    priceEur >= 0 &&
                    !(priceByn == 0 && priceUan == 0 && priceUsd == 0 && priceEur == 0)
            ) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }
}
