package com.epam.training.controllers;


import com.epam.training.controllers.utils.Constants;
import com.epam.training.controllers.utils.Utils;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.impl.PriceSoapServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import com.epam.training.service.PricePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.training.controllers.utils.Constants.*;

/**
 * Controller for prices
 * default url = "/v1/price";
 */
@RestController
@RequestMapping(value = "/" + Constants.VERSION_1_URL + "/" + Constants.PRICE_URL
        , produces = MediaType.APPLICATION_JSON_VALUE
)
public class PriceController {

    private static final String DEF_URL = "/" + Constants.VERSION_1_URL + "/" + Constants.PRICE_URL;
    private static final String SEARCH_PARAM_KEY = "searchParam";

    private PriceSoapServiceImpl priceService;
    private ProductH2ServiceImpl productService;

    @Autowired
    public PriceController(PriceSoapServiceImpl priceService, ProductH2ServiceImpl productService) {
        this.priceService = priceService;
        this.productService = productService;
    }

    @GetMapping(value = {"", "/page/{page}"}, produces = "application/json;charset=UTF-8")
    public ModelAndView getPrices(@PathVariable(required = false, name = "page") Integer page) {
        int pageNumber = Utils.getPageNumber(page);
        PricePage pricePage = priceService.getAll(pageNumber);
        ModelAndView mv = getViewSearchCheckErrors(pricePage);
        mv.addObject(PAGE_URL_KEY, DEF_URL + PAGE_KEY);
        return mv;
    }

    @GetMapping(value = "/{id}")
    public ModelAndView getPrice(
            @PathVariable("id") Long id
            , @RequestParam(required = false, name = "msg") String msg
    ) {
        ModelAndView mv = new ModelAndView("price/price");
        Price price = getPriceById(id);
        if (price == null) {
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        mv.addObject("price", price);
        mv.addObject("msg", msg);
        mv.addObject("priceForm", new Price());
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    @GetMapping(value = "/search")
    public ModelAndView getSearch() {
        return getViewSearch();
    }

    @GetMapping(value = {"/search/byCurrency", "/search/byCurrency/{page}"})
    public ModelAndView getSearchByCurrency(
            @RequestParam(required = false) String currencyValue
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        ModelAndView mv;
        if (currencyValue == null) {
            mv = getViewSearch();
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in currency field!");
            return mv;
        }
        int pageNumber = Utils.getPageNumber(page);
        Currency currency = Currency.valueOf(currencyValue);
        PricePage pricePage = priceService.getByCurrency(currency, pageNumber);
        mv = getViewSearchCheckErrors(pricePage);
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byCurrency/");
        mv.addObject(SEARCH_PARAM_KEY, "?currencyValue=" + currencyValue);
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    @GetMapping(value = {"/search/byProduct", "/search/byProduct/{page}"})
    public ModelAndView getSearchByProduct(
            @RequestParam(required = false) String productValue
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        ModelAndView mv;
        if (productValue == null) {
            mv = getViewSearch();
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in product field!");
            return mv;
        }
        int pageNumber = Utils.getPageNumber(page);
        Long id = Utils.getClearId(productValue);
        Product product = productService.getById(id);
        PricePage pricePage = priceService.getByProduct(product, pageNumber);
        mv = getViewSearchCheckErrors(pricePage);
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byProduct/");
        mv.addObject(SEARCH_PARAM_KEY, "?productValue=" + productValue);
        return mv;
    }

    @GetMapping(value = {"/search/byRange", "/search/byRange/{page}"})
    public ModelAndView getSearchByRange(
            @RequestParam(required = false) String currencyValue
            , @RequestParam(required = false) String minRange
            , @RequestParam(required = false) String maxRange
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        int min;
        int max;
        ModelAndView mv;
        if (currencyValue == null || minRange == null || maxRange == null) {
            mv = getViewSearch();
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in some field!");
            return mv;
        }
        try {
            min = Integer.parseInt(minRange);
            max = Integer.parseInt(maxRange);
        } catch (NumberFormatException e) {
            mv = getViewSearch();
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.addObject(ERROR_MESSAGE_KEY, "Incorrect data entered in price field!");
            return mv;
        }
        Currency currency = Currency.valueOf(currencyValue);
        int pageNumber = Utils.getPageNumber(page);
        PricePage pricePage = priceService.getByRange(currency, min, max, pageNumber);
        mv = getViewSearchCheckErrors(pricePage);
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byRange/");
        mv.addObject(SEARCH_PARAM_KEY,
                "?currencyValue=" + currencyValue
                        + "&minRange=" + minRange
                        + "&maxRange=" + maxRange);
        return mv;
    }

    @PutMapping(value = "/{id}")
    public ModelAndView updatePrice(
            @PathVariable Long id,
            @ModelAttribute("priceForm") @Validated Price priceForm
            , BindingResult result
            , final RedirectAttributes redirectAttributes
    ) {
        if (result.hasErrors() || priceForm.getValue() < 0) {
            return getViewError(id);
        }
        int value = priceForm.getValue();
        ModelAndView mv = new ModelAndView();
        Price price = getPriceById(id);
        if (price == null) {
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        price.setValue(value);
        priceService.editPrice(price);
        int page = getPageNumber(id);
        mv.setViewName("redirect:" + DEF_URL + PAGE_KEY + page);
        redirectAttributes.addAttribute(ERROR_MESSAGE_KEY, "Price update successfully!");
        return mv;
    }

    @DeleteMapping(value = "/{id}")
    public ModelAndView deleteProduct(@PathVariable Long id) {
        int pageNumber = getPageNumber(id);
        ModelAndView mv = new ModelAndView();

        try {
            priceService.delete(id);
        } catch (WebServiceException e) {
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        mv.setViewName("redirect:" + DEF_URL + PAGE_KEY + pageNumber);
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    private ModelAndView getViewError(Long id) {
        String errorMessage = "Incorrect data entered in some field!";
        return new ModelAndView("redirect:" + DEF_URL + "/" + id + "?msg=" + errorMessage);
    }

    private ModelAndView getViewSearch() {
        ModelAndView mv = new ModelAndView("price/price-search");
        mv.addObject("currency", Currency.values());
        mv.addObject("productsName", Utils.getProductNameList(productService));
        return mv;
    }

    private ModelAndView getViewSearchCheckErrors(PricePage pricePage) {
        ModelAndView mv = new ModelAndView();
        if (pricePage.getNumberOfElements() < 1) {
            mv = getViewSearch();
            mv.setStatus(HttpStatus.NOT_FOUND);
            mv.addObject(ERROR_MESSAGE_KEY, MSG_NOT_FOUND);
            return mv;
        }
        mv.setViewName("price/prices");
        mv.addObject("pricePage", pricePage);
        mv.addObject("pageNumber", pricePage.getPageNumber());
        mv.addObject("pricesList", changePrices(pricePage));
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    private List<Price> changePrices(PricePage pricePage) {
        List<com.epam.training.service.Price> prices = pricePage.getContent();
        List<Price> priceList = new ArrayList<>();
        for (com.epam.training.service.Price price : prices) {
            Price newPrice = new Price();
            newPrice.setValue(price.getValue());
            newPrice.setCurrency(Currency.valueOf(price.getCurrency().name()));
            newPrice.setProduct(productService.getById(price.getProductId()));
            newPrice.setId(price.getId());
            priceList.add(newPrice);
        }
        return priceList;
    }

    public int getPageNumber(Long id) {
        int pageNumber = 1;
        PricePage pricePage = priceService.getAll(pageNumber);
        int allPageCount = (int) pricePage.getTotalPages();
        for (int i = 1; i <= allPageCount; i++) {
            if (pricePage
                    .getContent()
                    .stream()
                    .anyMatch(p -> p.getId().equals(id))) {
                pageNumber = i;
                return pageNumber;
            }
            pricePage = priceService.getAll(i);
        }
        return pageNumber;
    }

    private Price getPriceById(Long id) {
        Price price;
        try {
            price = priceService.getById(id, productService);
        } catch (WebServiceException e) {
            price = null;
            return price;
        }
        return price;
    }
}
