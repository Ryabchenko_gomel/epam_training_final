package com.epam.training.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class StartController {

    @GetMapping("/")
    public ModelAndView startPage() {
        return new ModelAndView("index");
    }

    @GetMapping("/error-access")
    public ModelAndView errorAccessPage() {
        return new ModelAndView("error-access");
    }
}

