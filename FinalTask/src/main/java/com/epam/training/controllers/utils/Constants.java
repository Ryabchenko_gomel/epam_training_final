package com.epam.training.controllers.utils;

/**
 *  Class of constants
 */

public class Constants {

    private Constants(){
        //for sonar
    }

    public static final String PRODUCT_URL = "product";
    public static final String CATEGORY_URL = "category";
    public static final String PRICE_URL = "price";
    public static final String VERSION_1_URL = "v1";

    public static final String PAGE_URL_KEY = "pageUrl";
    public static final String PAGE_KEY = "/page/";
    public static final String ERROR_MESSAGE_KEY = "errmsg";
    public static final String MSG_NOT_FOUND = "Nothing found";

}
