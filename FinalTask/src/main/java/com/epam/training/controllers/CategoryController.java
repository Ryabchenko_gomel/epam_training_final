package com.epam.training.controllers;

import com.epam.training.controllers.utils.CategoryDtoValidator;
import com.epam.training.controllers.utils.Constants;
import com.epam.training.controllers.utils.Utils;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.dto.CategoryDto;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

import static com.epam.training.controllers.utils.Constants.*;

/**
 * Controller for categories
 * default url = "/v1/category";
 */
@RestController
@RequestMapping(value = "/" + Constants.VERSION_1_URL + "/" + Constants.CATEGORY_URL
        , produces = MediaType.APPLICATION_JSON_VALUE
)
public class CategoryController {

    private static final String DEF_URL = "/" + Constants.VERSION_1_URL + "/" + Constants.CATEGORY_URL;
    private static final String CATEGORY_NAME_KEY = "categoryName";
    private static final String CATEGORY_SEARCH_KEY = "category/category-search";
    private static final String MSG_INCORRECT_DATA = "Incorrect data entered in some field!";


    private CategoryH2ServiceImpl categoryService;
    private ProductH2ServiceImpl productService;

    @Autowired
    public CategoryController(ProductH2ServiceImpl productService, CategoryH2ServiceImpl categoryService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping(value = {"", "/page/{page}"}, produces = "application/json;charset=UTF-8")
    public ModelAndView getCategories(@PathVariable(required = false, name = "page") Integer page) {
        int pageNumber = Utils.getPageNumber(page);
        ModelAndView mv = getCategoriesView();
        Page<Category> categories = categoryService.getAll(pageNumber);
        mv.addObject(CATEGORY_NAME_KEY,
                Utils.getCategoryNameHierarchy(categoryService, "====", 0L));
        mv.addObject("categoriesPage", categories);
        mv.addObject(PAGE_URL_KEY, DEF_URL + PAGE_KEY);
        return mv;
    }

    @GetMapping(value = "/{id}")
    public ModelAndView getCategory(
            @PathVariable("id") Long id
            , @RequestParam(required = false, name = "msg") String msg
    ) {
        Category category = categoryService.getById(id);
        ModelAndView mv = new ModelAndView("category/category", "category", category);
        if (category == null) {
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        mv.addObject("categoryDtoForm", new CategoryDto());
        List<String> productsName = Utils.getProductNameList(productService);
        productsName.add("as it was");
        mv.addObject("productsName", productsName);
        List<String> categoryNameList = Utils.getCategoryNameHierarchy(categoryService, "===", id);
        categoryNameList.add("Empty");
        mv.addObject(CATEGORY_NAME_KEY, categoryNameList);

        mv.addObject("categoryHierarchy",
                Utils.getCategoryNameHierarchy(categoryService, "    ", 0L));
        mv.addObject("msg", msg);
        mv.setStatus(HttpStatus.OK);
        return mv;
    }

    @GetMapping(value = "/hierarchy")
    public ModelAndView getCategoriesHierarchy() {
        ModelAndView mv = new ModelAndView("category/categoriesHierarchy");
        mv.addObject("categoryHierarchy",
                Utils.getCategoryNameHierarchy(categoryService, "      ", 0L));
        return mv;
    }

    @GetMapping(value = "/search")
    public ModelAndView getSearch() {
        return new ModelAndView(CATEGORY_SEARCH_KEY);
    }


    @GetMapping(value = "/search/byId")
    public ModelAndView getSearchById(@RequestParam(required = false) String categoryId) {
        ModelAndView mv = new ModelAndView();
        long idLong = 0;
        try {
            idLong = Long.parseLong(categoryId);
        } catch (NumberFormatException e) {
            mv.addObject(ERROR_MESSAGE_KEY, MSG_INCORRECT_DATA);
            mv.setStatus(HttpStatus.BAD_REQUEST);
            mv.setViewName(CATEGORY_SEARCH_KEY);
            return mv;
        }
        Category categorySearch = categoryService.getById(idLong);
        return getSearchView(categorySearch);
    }


    @GetMapping(value = {"/search/byName", "/search/byName/{page}"})
    public ModelAndView getSearchByName(
            @RequestParam(required = false) String name
            , @PathVariable(required = false, name = "page") Integer page
    ) {
        int pageNumber = Utils.getPageNumber(page);
        ModelAndView mv = new ModelAndView();
        Page<Category> categories = categoryService.getByName(name, pageNumber);
        if (categories.getNumberOfElements() < 1) {
            mv.setViewName(CATEGORY_SEARCH_KEY);
            mv.setStatus(HttpStatus.NOT_FOUND);
            mv.addObject(ERROR_MESSAGE_KEY, MSG_NOT_FOUND);
            return mv;
        }
        mv = getCategoriesView();
        mv.addObject("categoriesPage", categories);
        mv.addObject(CATEGORY_NAME_KEY,
                Utils.getCategoryNameHierarchy(categoryService, "====", 0L));
        mv.addObject(PAGE_URL_KEY, DEF_URL + "/search/byName/");
        mv.addObject("searchParam",
                "?name=" + name);
        mv.setStatus(HttpStatus.OK);
        return mv;
    }


    @PostMapping(value = {"/0", "/{id}"})
    public ModelAndView addNewCategory(
            @ModelAttribute("categoryDtoForm") CategoryDto categoryDtoForm
            , final RedirectAttributes redirectAttributes
    ) {
        if (!CategoryDtoValidator.isValidate(categoryDtoForm)) {
            ModelAndView mv = new ModelAndView("redirect:/v1/category/");
            redirectAttributes.addFlashAttribute(ERROR_MESSAGE_KEY, MSG_INCORRECT_DATA);
            return mv;
        }
        Category category = categoryDtoForm.getCategory(categoryService, 0L);
        ModelAndView mv = new ModelAndView("redirect:" + DEF_URL
                + PAGE_KEY + Utils.getLastPageNumber(categoryService));
        categoryService.addCategory(category);
        categoryDtoForm.addProduct(category, productService);
        return mv;
    }

    @PutMapping(value = "/{id}")
    public ModelAndView updateCategory(
            @PathVariable Long id,
            @ModelAttribute("categoryDtoForm") CategoryDto categoryDtoForm
            , final RedirectAttributes redirectAttributes
    ) {
        if (!CategoryDtoValidator.isValidate(categoryDtoForm)) {
            String errorMessage = MSG_INCORRECT_DATA;
            return new ModelAndView("redirect:" + DEF_URL + "/" + id + "?msg=" + errorMessage);
        }
        ModelAndView mv = new ModelAndView();
        Category category = categoryDtoForm.getCategory(categoryService, id);
        category.setId(id);
        categoryService.editCategory(category);
        categoryDtoForm.deleteCategoryFromProducts(category, categoryService, productService);
        categoryDtoForm.addProduct(category, productService);
        int page = Utils.getPageNumber(categoryService, id);
        mv.setViewName("redirect:" + DEF_URL + PAGE_KEY + page);
        redirectAttributes.addAttribute(ERROR_MESSAGE_KEY, "Category update successfully!");
        return mv;
    }

    @DeleteMapping(value = "/{id}")
    public ModelAndView deleteCategory(@PathVariable Long id) {
        int pageNumber = Utils.getPageNumber(categoryService, id - 1);
        if (!categoryService.isExist(id)) {
            ModelAndView mv = new ModelAndView("/" + pageNumber);
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        //To remove this category from related products before deleting this category
        CategoryDto categoryDto = new CategoryDto();
        Category category = categoryService.getById(id);
        categoryDto.deleteCategoryFromProducts(category, categoryService, productService);
        //to move subcategories to a higher level
        categoryService.moveSubCategoryToSuperSuper(category);
        return Utils.deleteAndGetView(categoryService, id, pageNumber, DEF_URL);
    }

    private ModelAndView getCategoriesView() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("category/categories");
        mv.addObject("productsName", Utils.getProductNameList(productService));
        mv.addObject("categoryDtoForm", new CategoryDto());
        return mv;
    }

    private ModelAndView getSearchView(Category categorySearch) {
        ModelAndView mv = new ModelAndView();
        List<Category> categoryList = new ArrayList<>();
        if (categorySearch == null) {
            mv.setViewName(CATEGORY_SEARCH_KEY);
            mv.addObject(ERROR_MESSAGE_KEY, MSG_NOT_FOUND);
            mv.setStatus(HttpStatus.NOT_FOUND);
            return mv;
        }
        mv = getCategoriesView();
        categoryList.add(categorySearch);
        mv.addObject("categoriesList", categoryList);
        mv.addObject(CATEGORY_NAME_KEY,
                Utils.getCategoryNameHierarchy(categoryService, "====", 0L));
        return mv;
    }
}
