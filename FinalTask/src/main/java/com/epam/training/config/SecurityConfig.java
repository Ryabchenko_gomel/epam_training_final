package com.epam.training.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan("com.epam.training.controllers")
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
//                .password("123")
                .withUser("user")
                .password("$2a$04$CYNyAYm1kmHwxTuRHvLP/enybuQXZp4/DKpr6d/tvy.fO5HEF8Ix.")
                .roles("USER");
        auth.inMemoryAuthentication().withUser("manager")
//                .password("100500")
                .password("$2a$04$aRMB3v6ISVF8mJP9oi94U.5uMxRdxHZ0f6O7IHWQeLYNyjUxotWhy")
                .roles("MANAGER");
        auth.inMemoryAuthentication().withUser("admin")
//                .password("SYSADMIN")
                .password("$2a$04$4v4CrceFUlLyNHXfWLJIEuWOe3EPW.lneNZ.yMQKz3ZJdQKhSIS5W")
                .roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers("/v1/category/hierarchy").access("hasRole('ADMIN')")
                .antMatchers("/v1/price/**").access("hasRole('MANAGER') or hasRole('ADMIN')")
                .antMatchers("/v1").hasRole("USER")
                .antMatchers("/").permitAll()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout()
                .and()
                .httpBasic();


        // When the user has logged in as XX.
        // But access a page that requires role YY,
        // AccessDeniedException will throw.
        httpSecurity.authorizeRequests().and().exceptionHandling().accessDeniedPage("/error-access");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}