
package com.epam.training.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.training.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddPriceResponse_QNAME = new QName("http://service.training.epam.com/", "addPriceResponse");
    private final static QName _DeleteAllResponse_QNAME = new QName("http://service.training.epam.com/", "deleteAllResponse");
    private final static QName _DeleteByProductId_QNAME = new QName("http://service.training.epam.com/", "deleteByProductId");
    private final static QName _DeleteByProductIdResponse_QNAME = new QName("http://service.training.epam.com/", "deleteByProductIdResponse");
    private final static QName _GetByCurrencyPriceResponse_QNAME = new QName("http://service.training.epam.com/", "getByCurrencyPriceResponse");
    private final static QName _GetAllPage_QNAME = new QName("http://service.training.epam.com/", "getAllPage");
    private final static QName _GetByCurrencyPrice_QNAME = new QName("http://service.training.epam.com/", "getByCurrencyPrice");
    private final static QName _GetByProduct_QNAME = new QName("http://service.training.epam.com/", "getByProduct");
    private final static QName _GetById_QNAME = new QName("http://service.training.epam.com/", "getById");
    private final static QName _GetByProductResponse_QNAME = new QName("http://service.training.epam.com/", "getByProductResponse");
    private final static QName _GetByRange_QNAME = new QName("http://service.training.epam.com/", "getByRange");
    private final static QName _AddPrice_QNAME = new QName("http://service.training.epam.com/", "addPrice");
    private final static QName _DeleteAll_QNAME = new QName("http://service.training.epam.com/", "deleteAll");
    private final static QName _EditPrice_QNAME = new QName("http://service.training.epam.com/", "editPrice");
    private final static QName _GetAll_QNAME = new QName("http://service.training.epam.com/", "getAll");
    private final static QName _GetByIdResponse_QNAME = new QName("http://service.training.epam.com/", "getByIdResponse");
    private final static QName _GetByCurrency_QNAME = new QName("http://service.training.epam.com/", "getByCurrency");
    private final static QName _AddPriceParamResponse_QNAME = new QName("http://service.training.epam.com/", "addPriceParamResponse");
    private final static QName _GetAllResponse_QNAME = new QName("http://service.training.epam.com/", "getAllResponse");
    private final static QName _GetByCurrencyResponse_QNAME = new QName("http://service.training.epam.com/", "getByCurrencyResponse");
    private final static QName _AddPriceParam_QNAME = new QName("http://service.training.epam.com/", "addPriceParam");
    private final static QName _GetAllPageResponse_QNAME = new QName("http://service.training.epam.com/", "getAllPageResponse");
    private final static QName _GetByProductList_QNAME = new QName("http://service.training.epam.com/", "getByProductList");
    private final static QName _EditPriceResponse_QNAME = new QName("http://service.training.epam.com/", "editPriceResponse");
    private final static QName _GetByProductListResponse_QNAME = new QName("http://service.training.epam.com/", "getByProductListResponse");
    private final static QName _DeleteResponse_QNAME = new QName("http://service.training.epam.com/", "deleteResponse");
    private final static QName _GetByRangeResponse_QNAME = new QName("http://service.training.epam.com/", "getByRangeResponse");
    private final static QName _Delete_QNAME = new QName("http://service.training.epam.com/", "delete");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.training.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link GetByRangeResponse }
     * 
     */
    public GetByRangeResponse createGetByRangeResponse() {
        return new GetByRangeResponse();
    }

    /**
     * Create an instance of {@link EditPriceResponse }
     * 
     */
    public EditPriceResponse createEditPriceResponse() {
        return new EditPriceResponse();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link GetByProductListResponse }
     * 
     */
    public GetByProductListResponse createGetByProductListResponse() {
        return new GetByProductListResponse();
    }

    /**
     * Create an instance of {@link AddPriceParam }
     * 
     */
    public AddPriceParam createAddPriceParam() {
        return new AddPriceParam();
    }

    /**
     * Create an instance of {@link GetByProductList }
     * 
     */
    public GetByProductList createGetByProductList() {
        return new GetByProductList();
    }

    /**
     * Create an instance of {@link GetAllPageResponse }
     * 
     */
    public GetAllPageResponse createGetAllPageResponse() {
        return new GetAllPageResponse();
    }

    /**
     * Create an instance of {@link AddPriceParamResponse }
     * 
     */
    public AddPriceParamResponse createAddPriceParamResponse() {
        return new AddPriceParamResponse();
    }

    /**
     * Create an instance of {@link GetByCurrency }
     * 
     */
    public GetByCurrency createGetByCurrency() {
        return new GetByCurrency();
    }

    /**
     * Create an instance of {@link GetAll }
     * 
     */
    public GetAll createGetAll() {
        return new GetAll();
    }

    /**
     * Create an instance of {@link GetByIdResponse }
     * 
     */
    public GetByIdResponse createGetByIdResponse() {
        return new GetByIdResponse();
    }

    /**
     * Create an instance of {@link GetByCurrencyResponse }
     * 
     */
    public GetByCurrencyResponse createGetByCurrencyResponse() {
        return new GetByCurrencyResponse();
    }

    /**
     * Create an instance of {@link GetAllResponse }
     * 
     */
    public GetAllResponse createGetAllResponse() {
        return new GetAllResponse();
    }

    /**
     * Create an instance of {@link DeleteAll }
     * 
     */
    public DeleteAll createDeleteAll() {
        return new DeleteAll();
    }

    /**
     * Create an instance of {@link AddPrice }
     * 
     */
    public AddPrice createAddPrice() {
        return new AddPrice();
    }

    /**
     * Create an instance of {@link GetByRange }
     * 
     */
    public GetByRange createGetByRange() {
        return new GetByRange();
    }

    /**
     * Create an instance of {@link EditPrice }
     * 
     */
    public EditPrice createEditPrice() {
        return new EditPrice();
    }

    /**
     * Create an instance of {@link DeleteByProductIdResponse }
     * 
     */
    public DeleteByProductIdResponse createDeleteByProductIdResponse() {
        return new DeleteByProductIdResponse();
    }

    /**
     * Create an instance of {@link DeleteByProductId }
     * 
     */
    public DeleteByProductId createDeleteByProductId() {
        return new DeleteByProductId();
    }

    /**
     * Create an instance of {@link GetByProductResponse }
     * 
     */
    public GetByProductResponse createGetByProductResponse() {
        return new GetByProductResponse();
    }

    /**
     * Create an instance of {@link GetById }
     * 
     */
    public GetById createGetById() {
        return new GetById();
    }

    /**
     * Create an instance of {@link GetAllPage }
     * 
     */
    public GetAllPage createGetAllPage() {
        return new GetAllPage();
    }

    /**
     * Create an instance of {@link GetByCurrencyPrice }
     * 
     */
    public GetByCurrencyPrice createGetByCurrencyPrice() {
        return new GetByCurrencyPrice();
    }

    /**
     * Create an instance of {@link GetByProduct }
     * 
     */
    public GetByProduct createGetByProduct() {
        return new GetByProduct();
    }

    /**
     * Create an instance of {@link GetByCurrencyPriceResponse }
     * 
     */
    public GetByCurrencyPriceResponse createGetByCurrencyPriceResponse() {
        return new GetByCurrencyPriceResponse();
    }

    /**
     * Create an instance of {@link AddPriceResponse }
     * 
     */
    public AddPriceResponse createAddPriceResponse() {
        return new AddPriceResponse();
    }

    /**
     * Create an instance of {@link DeleteAllResponse }
     * 
     */
    public DeleteAllResponse createDeleteAllResponse() {
        return new DeleteAllResponse();
    }

    /**
     * Create an instance of {@link Price }
     * 
     */
    public Price createPrice() {
        return new Price();
    }

    /**
     * Create an instance of {@link PricePage }
     * 
     */
    public PricePage createPricePage() {
        return new PricePage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPriceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "addPriceResponse")
    public JAXBElement<AddPriceResponse> createAddPriceResponse(AddPriceResponse value) {
        return new JAXBElement<AddPriceResponse>(_AddPriceResponse_QNAME, AddPriceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "deleteAllResponse")
    public JAXBElement<DeleteAllResponse> createDeleteAllResponse(DeleteAllResponse value) {
        return new JAXBElement<DeleteAllResponse>(_DeleteAllResponse_QNAME, DeleteAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteByProductId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "deleteByProductId")
    public JAXBElement<DeleteByProductId> createDeleteByProductId(DeleteByProductId value) {
        return new JAXBElement<DeleteByProductId>(_DeleteByProductId_QNAME, DeleteByProductId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteByProductIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "deleteByProductIdResponse")
    public JAXBElement<DeleteByProductIdResponse> createDeleteByProductIdResponse(DeleteByProductIdResponse value) {
        return new JAXBElement<DeleteByProductIdResponse>(_DeleteByProductIdResponse_QNAME, DeleteByProductIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByCurrencyPriceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByCurrencyPriceResponse")
    public JAXBElement<GetByCurrencyPriceResponse> createGetByCurrencyPriceResponse(GetByCurrencyPriceResponse value) {
        return new JAXBElement<GetByCurrencyPriceResponse>(_GetByCurrencyPriceResponse_QNAME, GetByCurrencyPriceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getAllPage")
    public JAXBElement<GetAllPage> createGetAllPage(GetAllPage value) {
        return new JAXBElement<GetAllPage>(_GetAllPage_QNAME, GetAllPage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByCurrencyPrice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByCurrencyPrice")
    public JAXBElement<GetByCurrencyPrice> createGetByCurrencyPrice(GetByCurrencyPrice value) {
        return new JAXBElement<GetByCurrencyPrice>(_GetByCurrencyPrice_QNAME, GetByCurrencyPrice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByProduct")
    public JAXBElement<GetByProduct> createGetByProduct(GetByProduct value) {
        return new JAXBElement<GetByProduct>(_GetByProduct_QNAME, GetByProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getById")
    public JAXBElement<GetById> createGetById(GetById value) {
        return new JAXBElement<GetById>(_GetById_QNAME, GetById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByProductResponse")
    public JAXBElement<GetByProductResponse> createGetByProductResponse(GetByProductResponse value) {
        return new JAXBElement<GetByProductResponse>(_GetByProductResponse_QNAME, GetByProductResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByRange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByRange")
    public JAXBElement<GetByRange> createGetByRange(GetByRange value) {
        return new JAXBElement<GetByRange>(_GetByRange_QNAME, GetByRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPrice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "addPrice")
    public JAXBElement<AddPrice> createAddPrice(AddPrice value) {
        return new JAXBElement<AddPrice>(_AddPrice_QNAME, AddPrice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "deleteAll")
    public JAXBElement<DeleteAll> createDeleteAll(DeleteAll value) {
        return new JAXBElement<DeleteAll>(_DeleteAll_QNAME, DeleteAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditPrice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "editPrice")
    public JAXBElement<EditPrice> createEditPrice(EditPrice value) {
        return new JAXBElement<EditPrice>(_EditPrice_QNAME, EditPrice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getAll")
    public JAXBElement<GetAll> createGetAll(GetAll value) {
        return new JAXBElement<GetAll>(_GetAll_QNAME, GetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByIdResponse")
    public JAXBElement<GetByIdResponse> createGetByIdResponse(GetByIdResponse value) {
        return new JAXBElement<GetByIdResponse>(_GetByIdResponse_QNAME, GetByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByCurrency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByCurrency")
    public JAXBElement<GetByCurrency> createGetByCurrency(GetByCurrency value) {
        return new JAXBElement<GetByCurrency>(_GetByCurrency_QNAME, GetByCurrency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPriceParamResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "addPriceParamResponse")
    public JAXBElement<AddPriceParamResponse> createAddPriceParamResponse(AddPriceParamResponse value) {
        return new JAXBElement<AddPriceParamResponse>(_AddPriceParamResponse_QNAME, AddPriceParamResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getAllResponse")
    public JAXBElement<GetAllResponse> createGetAllResponse(GetAllResponse value) {
        return new JAXBElement<GetAllResponse>(_GetAllResponse_QNAME, GetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByCurrencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByCurrencyResponse")
    public JAXBElement<GetByCurrencyResponse> createGetByCurrencyResponse(GetByCurrencyResponse value) {
        return new JAXBElement<GetByCurrencyResponse>(_GetByCurrencyResponse_QNAME, GetByCurrencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPriceParam }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "addPriceParam")
    public JAXBElement<AddPriceParam> createAddPriceParam(AddPriceParam value) {
        return new JAXBElement<AddPriceParam>(_AddPriceParam_QNAME, AddPriceParam.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getAllPageResponse")
    public JAXBElement<GetAllPageResponse> createGetAllPageResponse(GetAllPageResponse value) {
        return new JAXBElement<GetAllPageResponse>(_GetAllPageResponse_QNAME, GetAllPageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByProductList")
    public JAXBElement<GetByProductList> createGetByProductList(GetByProductList value) {
        return new JAXBElement<GetByProductList>(_GetByProductList_QNAME, GetByProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditPriceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "editPriceResponse")
    public JAXBElement<EditPriceResponse> createEditPriceResponse(EditPriceResponse value) {
        return new JAXBElement<EditPriceResponse>(_EditPriceResponse_QNAME, EditPriceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByProductListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByProductListResponse")
    public JAXBElement<GetByProductListResponse> createGetByProductListResponse(GetByProductListResponse value) {
        return new JAXBElement<GetByProductListResponse>(_GetByProductListResponse_QNAME, GetByProductListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "deleteResponse")
    public JAXBElement<DeleteResponse> createDeleteResponse(DeleteResponse value) {
        return new JAXBElement<DeleteResponse>(_DeleteResponse_QNAME, DeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByRangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "getByRangeResponse")
    public JAXBElement<GetByRangeResponse> createGetByRangeResponse(GetByRangeResponse value) {
        return new JAXBElement<GetByRangeResponse>(_GetByRangeResponse_QNAME, GetByRangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Delete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.training.epam.com/", name = "delete")
    public JAXBElement<Delete> createDelete(Delete value) {
        return new JAXBElement<Delete>(_Delete_QNAME, Delete.class, null, value);
    }

}
