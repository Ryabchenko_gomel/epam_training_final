
package com.epam.training.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pricePage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pricePage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="content" type="{http://service.training.epam.com/}price" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="hasNext" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="hasPrevious" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="numberOfElements" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalPages" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pricePage", propOrder = {
    "content",
    "hasNext",
    "hasPrevious",
    "numberOfElements",
    "pageNumber",
    "totalPages"
})
public class PricePage {

    @XmlElement(nillable = true)
    protected List<Price> content;
    protected boolean hasNext;
    protected boolean hasPrevious;
    protected int numberOfElements;
    protected int pageNumber;
    protected long totalPages;

    /**
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Price }
     * 
     * 
     */
    public List<Price> getContent() {
        if (content == null) {
            content = new ArrayList<Price>();
        }
        return this.content;
    }

    /**
     * Gets the value of the hasNext property.
     * 
     */
    public boolean isHasNext() {
        return hasNext;
    }

    /**
     * Sets the value of the hasNext property.
     * 
     */
    public void setHasNext(boolean value) {
        this.hasNext = value;
    }

    /**
     * Gets the value of the hasPrevious property.
     * 
     */
    public boolean isHasPrevious() {
        return hasPrevious;
    }

    /**
     * Sets the value of the hasPrevious property.
     * 
     */
    public void setHasPrevious(boolean value) {
        this.hasPrevious = value;
    }

    /**
     * Gets the value of the numberOfElements property.
     * 
     */
    public int getNumberOfElements() {
        return numberOfElements;
    }

    /**
     * Sets the value of the numberOfElements property.
     * 
     */
    public void setNumberOfElements(int value) {
        this.numberOfElements = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     */
    public long getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     */
    public void setTotalPages(long value) {
        this.totalPages = value;
    }

}
