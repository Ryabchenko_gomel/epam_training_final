package com.epam.training.model.restservice;

import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import com.epam.training.service.PricePage;

import javax.xml.ws.WebServiceException;
import java.util.List;


/**
 * Price Data Access Interface
 */
public interface PriceService {
    Price addPrice(Price price);

    boolean delete(Long id);

    void deleteByProductId(Long productId);

    void deleteAll();

    Price getById(Long id, ProductH2ServiceImpl productH2Service) throws WebServiceException;

    PricePage getByCurrency(Currency currency, int page);

    PricePage getByCurrencyValue(Currency currency, int value, int page);

    PricePage getByProduct(Product product, int page);

    PricePage getByRange(Currency currency, int start, int end, int page);

    void editPrice(Price price);

    List<Price> getAll();

    PricePage getAll(int page);

    List<com.epam.training.service.Price> getListByProduct(Product product);

}
