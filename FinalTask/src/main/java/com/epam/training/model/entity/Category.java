package com.epam.training.model.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Category Description Class
 */
@Entity
public class Category extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "categories"
            , fetch = FetchType.LAZY)
    private Set<Product> products = new HashSet<>();


    @OneToMany(fetch = FetchType.LAZY
            , mappedBy = "superCategory"
            , orphanRemoval = true)
    @Cascade({
            org.hibernate.annotations.CascadeType.MERGE
            , org.hibernate.annotations.CascadeType.SAVE_UPDATE
    })
    private Set<Category> subCategories = new HashSet<>();

    @ManyToOne
    private Category superCategory;


    public Category() {
        //for Sonar
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Set<Category> subCategories) {
        this.subCategories = subCategories;
    }

    public void addSubCategory(Category subCategory) {
        this.subCategories.add(subCategory);
    }

    public Category getSuperCategory() {
        return superCategory;
    }

    public void setSuperCategory(Category superCategory) {
        this.superCategory = superCategory;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return getId().equals(category.getId()) &&
                getName().equals(category.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "\n\nCategory{" +
                "\tid=\t" + id +
                ",\tname=\t'" + name + '\'' +
                //superCategory and subCategory
                ", " + getCategoriesString() +
                ",\n\t\tProducts=\t" + getProductsString() +
                '}';
    }

    private String getProductsString() {
        if (products == null || products.isEmpty()) {
            return "EMPTY";
        }
        StringBuilder result = new StringBuilder();
        result.append("\t");
        for (Product product : products) {
            result.append(product.getName())
                    .append("; ")
                    .append("\t");
        }
        return result.toString();
    }

    private String getCategoriesString() {
        StringBuilder result = new StringBuilder();
        if (superCategory == null) {
            result.append("\t\tsuperCategory= empty     ");
        } else {
            result.append("\t\tsuperCategory= ").append(superCategory.getName());
        }
        if (subCategories == null || subCategories.isEmpty()) {
            result.append("\t\tsubCategory=  empty");
        } else {
            result.append("\t\tsubCategory= ");
            for (Category category : subCategories) {
                result.append(category.getName())
                        .append("; ")
                        .append("\t");
            }
        }
        return result.toString();
    }
}
