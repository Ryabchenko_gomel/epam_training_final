package com.epam.training.model.entity;

import java.util.Objects;


/**
 * Price Description Class
 */
public class Price {
    private Long id;

    private int value;

    private Currency currency;

    private Product product;

    public Price() {
    }

    public Price(int value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(int price) {
        this.value = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Price)) return false;
        Price price1 = (Price) o;
        return getValue() == price1.getValue() &&
                getId().equals(price1.getId()) &&
                getCurrency() == price1.getCurrency() &&
                Objects.equals(getProduct(), price1.getProduct());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getValue(), getCurrency(), getProduct());
    }

    @Override
    public String toString() {
        return "\nPrice{" +
                "\t\tid=" + id +
                ", \t\tprice=" + value +
                ", \t\tcurrency=" + currency +
                ", \t\tproduct=" + (product==null ? "Empty" : product.getName() ) +
                '}';
    }

}
