package com.epam.training.model.utils;

import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.CategoryService;
import com.epam.training.model.restservice.PriceService;
import com.epam.training.model.restservice.ProductService;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.PriceSoapServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.lang.invoke.MethodHandles;
import java.util.*;

/**
 * Random Data Generator for Database Startup
 */
public class Generator {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
    private Random random = new Random();
    private int productCount = 1;
    private int categoryCount = 1;

    private ApplicationContext context;
    private int productNumber;

    private int categoryNumber;

    public Generator() {
    }

    public Generator(ApplicationContext context) {
        this.context = context;
    }

    public void setDB(String categoryNumberStr, String productNumberStr) {
        try {
            this.categoryNumber = Integer.parseInt(categoryNumberStr);
            this.productNumber = Integer.parseInt(productNumberStr);
        } catch (NumberFormatException | NullPointerException e) {
            LOG.warn("Invalid data in file /resources/app.properties for var 'productNumber' or 'categoryNumber' ", e);
        }

        LOG.info("Set data in DB start");

        ProductService productService = context.getBean(ProductH2ServiceImpl.class);
        CategoryService categoryService = context.getBean(CategoryH2ServiceImpl.class);
        PriceService priceService = context.getBean(PriceSoapServiceImpl.class);

        categoryService.deleteAll();
        priceService.deleteAll();
        productService.deleteAll();

        List<Category> categories = getAllCategories();
        categories.forEach(categoryService::addCategory);
        Set<Price> priceSet;
        for (int i = 0; i < productNumber; i++) {
            priceSet = getSetPrices();
            Category category = categories.get(random.nextInt(categoryCount - 1));
            Set<Category> categorySet = new HashSet<>();
            categorySet.add(category);

            if (category.getSuperCategory() != null) {
                Category firstSuperCategory = category.getSuperCategory();
                categorySet.add(firstSuperCategory);
                if (firstSuperCategory.getSuperCategory() != null) {
                    categorySet.add(firstSuperCategory.getSuperCategory());
                }
            }
            Product product = getProduct(priceSet, categorySet);

            productService.addProduct(product);
        }
        LOG.info("Set data in DB end successful");
    }

    private Product getProduct(Set<Price> prices, Set<Category> categories) {
        Product product = new Product();
        String productName = "product_" + productCount;
        productCount++;
        product.setName(productName);
        product.setCategories(categories);
        product.setPrices(prices);
        return product;
    }

    private Set<Price> getSetPrices() {
        Set<Price> prices = new HashSet<>();
        for (int i = 0; i <= (random.nextInt(4)); i++) {
            prices.add(getPrice(i));
        }
        return prices;
    }

    private Price getPrice(int currencyIndex) {
        Price price = new Price();
        price.setCurrency(Currency.values()[currencyIndex]);
        price.setValue(random.nextInt(5) + 1);
        return price;
    }

    private List<Category> getAllCategories() {
        List<Category> categorySet = new ArrayList<>();
        Category category;
        int indexFirstLayer = categoryNumber / 9;
        int indexSecondLayer = categoryNumber / 4;
        int indexThirdLayer = categoryNumber / 2;
        for (int i = 1; i <= indexFirstLayer; i++) {
            category = getCategory();
            categorySet.add(category);
        }
        addNewLayerCategory(categorySet, 0, indexFirstLayer, indexSecondLayer);
        addNewLayerCategory(categorySet, indexFirstLayer, indexSecondLayer, indexThirdLayer);
        return categorySet;
    }

    private void addNewLayerCategory(List<Category> categorySet, int startIndex, int endIndex, int layerIndex) {
        Category category;
        for (int i = 1; i <= layerIndex; i++) {
            int superIndex = random.nextInt(endIndex) + startIndex;
            category = getCategory();
            Category superCategory = categorySet.get(superIndex);
            category.setSuperCategory(superCategory);
            superCategory.addSubCategory(category);
            categorySet.set(superIndex, superCategory);
            categorySet.add(category);
        }
    }

    private Category getCategory() {
        Category category = new Category();
        String categoryName = "category_" + categoryCount;
        categoryCount++;
        category.setName(categoryName);
        return category;
    }
}
