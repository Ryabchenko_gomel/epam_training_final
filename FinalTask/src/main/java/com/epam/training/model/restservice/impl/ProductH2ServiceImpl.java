package com.epam.training.model.restservice.impl;

import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.repository.ProductRepository;
import com.epam.training.model.restservice.ProductService;
import com.epam.training.model.utils.PriceConvertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service to access Product data
 */
@Service
public class ProductH2ServiceImpl implements ProductService {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    @Resource
    private Environment env;
    private int paginationSize = 5;
    private static final String KEY_PAGINATION_SIZE = "paginationSize";


    private ProductRepository productRepository;
    private PriceSoapServiceImpl priceSoapService;

    @Autowired
    public ProductH2ServiceImpl(ProductRepository productRepository, PriceSoapServiceImpl priceSoapService) {
        this.priceSoapService = priceSoapService;
        this.productRepository = productRepository;
    }

    @PostConstruct
    private void init() {
        try {
            this.paginationSize = Integer.parseInt(env.getRequiredProperty(KEY_PAGINATION_SIZE));
        } catch (NumberFormatException e) {
            LOG.error("Incorrect data in file-'/resources/app.properties'  field-'paginationSize' ", e);
        }
    }

    @Override
    public Product addProduct(Product product) {
        Product saveProduct = productRepository.save(product);
        for (Price price : saveProduct.getPrices()) {
            price.setProduct(product);
            priceSoapService.addPrice(price);
        }
        return saveProduct;
    }

    @Override
    public boolean delete(Long id) {
        try {
            if (productRepository.existsById(id)) {
                priceSoapService.deleteByProductId(id);
                productRepository.deleteById(id);
            } else {
                return false;
            }
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Product product) {
        try {
            priceSoapService.deleteByProductId(product.getId());
            productRepository.delete(product);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    @Override
    public Product getByName(String name) {
        Product product = productRepository.findByName(name);
        Set<Price> priceSet = addPrices(priceSoapService.getListByProduct(product), product);
        product.setPrices(priceSet);
        return productRepository.findByName(name);
    }

    @Override
    public Page<Product> getByName(String name, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, paginationSize);
        Page<Product> productPage = productRepository.findByName(name, pageable);
        return addPrices(productPage);
    }

    @Override
    public Product getById(Long id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product != null) {
            Set<Price> priceSet = addPrices(priceSoapService.getListByProduct(product), product);
            product.setPrices(priceSet);
        }
        return product;
    }

    @Override
    public Page<Product> getByCategoryId(Long id, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, paginationSize);
        Page<Product> productPage = productRepository.findByCategoriesId(id, pageable);
        return addPrices(productPage);
    }

    @Override
    public Product editProduct(Product product) {
        Product saveProduct = productRepository.save(product);
        priceSoapService.deleteByProductId(product.getId());
        for (Price price : product.getPrices()) {
            price.setProduct(product);
            priceSoapService.addPrice(price);
        }
        return saveProduct;
    }

    @Override
    public Page<Product> getAll(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, paginationSize);
        Page<Product> page = productRepository.findAll(pageable);
        return addPrices(page);
    }

    @Override
    public List<Product> getAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public void deleteAll() {
        productRepository.deleteAll();
    }


    private Set<Price> addPrices(List<com.epam.training.service.Price> prices, Product product) {
        Set<Price> priceSet = new HashSet<>();
        for (com.epam.training.service.Price price : prices) {
            Price newPrice = PriceConvertService.convertServicePriceToEntityPrice(price, product);
            priceSet.add(newPrice);
        }
        return priceSet;
    }

    private Page<Product> addPrices(Page<Product> productPage) {
        for (Product product : productPage.getContent()) {
            List<com.epam.training.service.Price> priceServiceList = priceSoapService.getListByProduct(product);
            Set<Price> priceSet = addPrices(priceServiceList, product);
            product.setPrices(priceSet);
        }
        return productPage;
    }
}
