package com.epam.training.model.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Product Description Class
 */
@Entity
public class Product extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(
            fetch = FetchType.LAZY

    )
    @JoinTable(name = "CATEGORY_PRODUCT",
            joinColumns = @JoinColumn(name = "PRODUCT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CATEGORY_ID")
    )
    @Cascade({
            org.hibernate.annotations.CascadeType.MERGE
            , org.hibernate.annotations.CascadeType.SAVE_UPDATE
    })
    private Set<Category> categories = new HashSet<>();
    @Transient
    private Set<Price> prices = new HashSet<>();

    public Product() {
        //for Sonar
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public void addCategory(Category category) {
        this.categories.add(category);
    }

    public void removeCategory(Category category) {
        this.categories.remove(category);
    }


    public Set<Price> getPrices() {
        return prices;
    }

    public List<Price> getPricesList() {
        return prices.stream().collect(Collectors.toList());
    }

    public void setPrices(Set<Price> prices) {
        this.prices = prices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId().equals(product.getId()) &&
                getName().equals(product.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "\nProduct{" +
                "\tid=" + id +
                ",\t\tname='" + name + '\'' +
                ",\t\t prices=" + getPricesString() +
                ",\t\t\t\t Categories=" + getCategoriesString() +

                '}';
    }

    private String getCategoriesString() {
        if (categories == null || categories.isEmpty()) {
            return "EMPTY";
        }
        StringBuilder result = new StringBuilder();
        result.append("\t");
        for (Category category : categories) {
            result.append(category.getName())
                    .append("; ")
                    .append("\t");
        }
        return result.toString();
    }

    private String getPricesString() {
        if (prices == null || prices.isEmpty()) {
            return "EMPTY";
        }
        StringBuilder result = new StringBuilder();
        result.append("\t");
        for (Price price : prices) {
            result.append(price.getCurrency())
                    .append("=")
                    .append(price.getValue())
                    .append("\t");
        }
        return result.toString();
    }


}
