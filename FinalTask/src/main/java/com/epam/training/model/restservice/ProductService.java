package com.epam.training.model.restservice;

import com.epam.training.model.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * Product Data Access Interface
 */
public interface ProductService extends BaseService {
    Product addProduct(Product product);

    boolean delete(Long id);

    boolean delete(Product product);

    void deleteAll();

    Product getByName(String name);

    Page<Product> getByName(String name, int pageNumber);

    Product getById(Long id);




    Page<Product> getByCategoryId(Long id, int pageNumber);

    Product editProduct(Product product);

    Page<Product> getAll(int pageNumber);

    List<Product> getAll();

}
