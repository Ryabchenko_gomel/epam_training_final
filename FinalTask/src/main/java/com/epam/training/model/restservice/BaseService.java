package com.epam.training.model.restservice;

import org.springframework.data.domain.Page;

/**
 * Basic interface for creating hierarchies
 * for writing polymorphic methods
 */
public interface BaseService {

    boolean delete(Long id);

    void deleteAll();

    Object getById(Long id);

    <T> Page<T> getAll(int pageNumber);

}
