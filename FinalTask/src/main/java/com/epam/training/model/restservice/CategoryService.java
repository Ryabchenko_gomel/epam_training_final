package com.epam.training.model.restservice;

import com.epam.training.model.entity.Category;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Category Data Access Interface
 */
public interface CategoryService extends BaseService{
    Category addCategory(Category category);

    boolean delete(Long id);

    void deleteAll();

    Page<Category> getByName(String name, int pageNumber);

    Category getByName(String name);

    Category getById(Long id);

    Category editCategory(Category category);

    Page<Category> getAll(int pageNumber);

    List<Category> getAll();
}
