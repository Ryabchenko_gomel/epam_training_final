package com.epam.training.model.entity.dto;

import com.epam.training.controllers.utils.Utils;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;


/**
 * Class for transferring and converting
 * data from the input form to the Product
 */
public class ProductDto {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    private String name;
    private Set<String> categories;
    private String priceBYN;
    private String priceUAN;
    private String priceUSD;
    private String priceEUR;

    private int priceIntBYN;
    private int priceIntUAN;
    private int priceIntUSD;
    private int priceIntEUR;

    public ProductDto() {
        //for sonar
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    public String getPriceBYN() {
        return priceBYN;
    }

    public void setPriceBYN(String priceBYN) {
        this.priceBYN = priceBYN;
    }

    public String getPriceUAN() {
        return priceUAN;
    }

    public void setPriceUAN(String priceUAN) {
        this.priceUAN = priceUAN;
    }

    public String getPriceUSD() {
        return priceUSD;
    }

    public void setPriceUSD(String priceUSD) {
        this.priceUSD = priceUSD;
    }

    public String getPriceEUR() {
        return priceEUR;
    }

    public void setPriceEUR(String priceEUR) {
        this.priceEUR = priceEUR;
    }

    public Product getProduct(CategoryH2ServiceImpl categoryService) {
        Product product = new Product();
        product.setName(this.name);
        parsePrices();
        product.setPrices(getSetPrices(product));
        product.setCategories(getSetCategory(categoryService));
        return product;
    }

    private void parsePrices() {
        priceIntBYN = parsePrice(priceBYN, "BYN");
        priceIntUAN = parsePrice(priceUAN, "UAN");
        priceIntUSD = parsePrice(priceUSD, "USD");
        priceIntEUR = parsePrice(priceEUR, "EUR");
    }

    private int parsePrice(String priceField, String errorName) {
        int price = 0;
        try {
            if (!priceField.isEmpty()) {
                price = Integer.parseInt(priceField);
            }
        } catch (NumberFormatException e) {
            LOG.warn("Error input for {0} {1}", errorName, e);
        }
        return price;
    }

    public Set<Price> getSetPrices(Product product) {
        Set<Price> priceSet = new HashSet<>();
        addPrice(priceSet, product, priceIntBYN, Currency.BYN);
        addPrice(priceSet, product, priceIntUAN, Currency.UAN);
        addPrice(priceSet, product, priceIntUSD, Currency.USD);
        addPrice(priceSet, product, priceIntEUR, Currency.EUR);
        return priceSet;
    }

    private void addPrice(Set<Price> priceSet, Product product, Integer priceInt, Currency currency) {
        if (priceInt != 0) {
            Price price = new Price(priceInt, currency);
            price.setProduct(product);
            priceSet.add(price);
        }
    }

    private Set<Category> getSetCategory(CategoryH2ServiceImpl categoryService) {
        Set<Category> categorySet = new HashSet<>();
        for (String categoryName : categories) {
            Long id = Utils.getClearId(categoryName);
            Category category = categoryService.getById(id);
            categorySet.add(category);
            addSuperCategory(categorySet, category);
        }
        return categorySet;
    }

    private void addSuperCategory(Set<Category> categorySet, Category category) {
        if (category.getSuperCategory() != null) {
            categorySet.add(category.getSuperCategory());
            addSuperCategory(categorySet, category.getSuperCategory());
        }
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "name='" + name + '\'' +
                ", categories=" + categories +
                ", priceBYN=" + priceBYN +
                ", priceUAN=" + priceUAN +
                ", priceUSD=" + priceUSD +
                ", priceEUR=" + priceEUR +
                '}';
    }
}
