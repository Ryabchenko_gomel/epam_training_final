package com.epam.training.model.restservice.impl;

import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.repository.ProductRepository;
import com.epam.training.model.restservice.PriceService;
import com.epam.training.model.utils.PriceConvertService;
import com.epam.training.service.PriceH2ServiceService;
import com.epam.training.service.PricePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.xml.ws.WebServiceException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

/**
 * Service to access Price data from SOAP service
 */
@Service
public class PriceSoapServiceImpl implements PriceService {
    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    @Resource
    private Environment env;
    private int paginationSize = 5;
    private static final String KEY_PAGINATION_SIZE = "paginationSize";

    private com.epam.training.service.PriceService priceService;

    private ProductRepository productRepository;

    @Autowired
    public PriceSoapServiceImpl(ProductRepository productRepository) {
        PriceH2ServiceService priceH2ServiceService = new PriceH2ServiceService();
        this.priceService = priceH2ServiceService.getPriceH2ServicePort();
        this.productRepository = productRepository;
    }

    @PostConstruct
    private void init() {
        try {
            this.paginationSize = Integer.parseInt(env.getRequiredProperty(KEY_PAGINATION_SIZE));
        } catch (NumberFormatException e) {
            LOG.error("Incorrect data in file-'/resources/app.properties'  field-'paginationSize' ", e);
        }
    }


    @Override
    public Price addPrice(Price price) {
        priceService.addPriceParam(price.getValue(), price.getCurrency().getName(), price.getProduct().getId());
        return price;
    }

    @Override
    public boolean delete(Long id) throws WebServiceException {
        return priceService.delete(id);
    }

    @Override
    public void deleteByProductId(Long productId) {
        priceService.deleteByProductId(productId);
    }

    @Override
    public void deleteAll() {
        priceService.deleteAll();
    }

    @Override
    public Price getById(Long id, ProductH2ServiceImpl productH2Service) throws WebServiceException {
        com.epam.training.service.Price priceS = priceService.getById(id);
        Product product = productH2Service.getById(priceS.getProductId());
        return PriceConvertService.convertServicePriceToEntityPrice(priceS, product);
    }


    @Override
    public PricePage getByCurrency(Currency currency, int page) {
        return priceService.getByCurrency(currency.getName(), page + 1, paginationSize);
    }

    @Override
    public PricePage getByCurrencyValue(Currency currency, int value, int page) {
        return priceService.getByCurrencyPrice(currency.getName(), value, page + 1, paginationSize);
    }

    @Override
    public PricePage getByProduct(Product product, int page) {
        return priceService.getByProduct(product.getId(), page + 1, paginationSize);
    }

    @Override
    public PricePage getByRange(Currency currency, int start, int end, int page) {
        return priceService.getByRange(currency.getName(), start, end, page + 1, paginationSize);
    }

    @Override
    public void editPrice(Price price) {
        priceService.editPrice(price.getId(), price.getValue(), price.getCurrency().getName(), price.getProduct().getId());
    }

    @Override
    public List<Price> getAll() {
        List<com.epam.training.service.Price> priceServiceList = priceService.getAll();
        List<Price> priceList = new ArrayList<>();
        for (com.epam.training.service.Price price : priceServiceList) {
            Product product = productRepository.findById(price.getProductId()).orElse(null);
            Price newPrice = PriceConvertService.convertServicePriceToEntityPrice(price, product);
            priceList.add(newPrice);
        }
        return priceList;
    }

    @Override
    public PricePage getAll(int page) {
        return priceService.getAllPage(page, paginationSize);
    }

    @Override
    public List<com.epam.training.service.Price> getListByProduct(Product product) {
        return priceService.getByProductList(product.getId());
    }

}
