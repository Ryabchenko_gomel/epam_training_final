package com.epam.training.model.utils;

import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;

/**
 * Utility class for converting data received from the SOAP service
 */
public class PriceConvertService {
    private PriceConvertService() {
        //for sonar
    }

    public static Price convertServicePriceToEntityPrice (com.epam.training.service.Price priceS, Product product){
        Price newPrice = new Price();
        newPrice.setValue(priceS.getValue());
        newPrice.setCurrency(Currency.valueOf(priceS.getCurrency().name()));
        newPrice.setProduct(product);
        newPrice.setId(priceS.getId());
        return newPrice;
    }
}
