package com.epam.training.model.entity.dto;

import com.epam.training.controllers.utils.Utils;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;

import java.util.HashSet;
import java.util.Set;

/**
 * Class for transferring and converting
 * data from the input form to the Category
 */
public class CategoryDto {

    private String name;
    private Set<String> products;
    private String superCategory;
    private Set<String> subCategories;


    public CategoryDto() {
        //for sonar
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getProducts() {
        return products;
    }

    public void setProducts(Set<String> products) {
        this.products = products;
    }

    public Set<String> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Set<String> subCategories) {
        this.subCategories = subCategories;
    }

    public String getSuperCategory() {
        return superCategory;
    }

    public void setSuperCategory(String superCategory) {
        this.superCategory = superCategory;
    }

    public Category getCategory(CategoryH2ServiceImpl categoryService, Long originalId) {
        Category category = new Category();
        category.setName(name);
        if (superCategory != null && !superCategory.equals("Empty")) {
            Long superId = Utils.getClearId(superCategory);
            Category superCategoryDb = categoryService.getById(superId);
            category.setSuperCategory(superCategoryDb);
        }
        if (products.stream().anyMatch(p -> p.equals("as it was"))) {
            Set<Product> productSet = categoryService.getById(originalId).getProducts();
            products.clear();
            for (Product product : productSet) {
                products.add(product.getName() + " (" + product.getId() + ")");
            }
        }
        return category;
    }

    public void addProduct(Category category, ProductH2ServiceImpl productService) {
        Set<Product> productSet = new HashSet<>();
        if (!products.isEmpty()) {
            for (String productName : products) {
                Long id = Utils.getClearId(productName);
                Product product = productService.getById(id);
                product.addCategory(category);
                productService.editProduct(product);
                productSet.add(product);
            }
            category.setProducts(productSet);
        }

    }

    public void deleteCategoryFromProducts(Category category, CategoryH2ServiceImpl categoryService, ProductH2ServiceImpl productService) {
        Set<Product> productSet = new HashSet<>();
        category.setProducts(productSet);
        Set<Product> productOtherSet = categoryService.getById(category.getId()).getProducts();
        for (Product product : productOtherSet) {
            product.removeCategory(category);
            productService.editProduct(product);
        }
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "name='" + name + '\'' +
                ", products=" + products +
                ", subCategories=" + subCategories +
                ", superCategory='" + superCategory + '\'' +
                '}';
    }
}
