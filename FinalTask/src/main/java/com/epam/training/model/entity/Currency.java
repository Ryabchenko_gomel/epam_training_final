package com.epam.training.model.entity;


/**
 * Enumeration of currency
 */
public enum Currency {
    BYN("BYN"),
    UAN("UAN"),
    USD("USD"),
    EUR("EUR");

    private final String name;

    private Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
