package com.epam.training.model.repository;

import com.epam.training.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to get access table PRODUCT
 */

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Product findByName(String name);

    Page<Product> findByName(String name, Pageable pageable);

    Page<Product> findByCategoriesId(Long id, Pageable pageable);

    Page<Product> findAll(Pageable pageable);
}
