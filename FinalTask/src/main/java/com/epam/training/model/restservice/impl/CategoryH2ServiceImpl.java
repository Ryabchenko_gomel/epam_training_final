package com.epam.training.model.restservice.impl;

import com.epam.training.model.entity.Category;
import com.epam.training.model.repository.CategoryRepository;
import com.epam.training.model.restservice.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Set;


/**
 * Service to access category data
 */
@Service
public class CategoryH2ServiceImpl implements CategoryService {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    @Resource
    private Environment env;
    private int paginationSize = 5;
    private static final String KEY_PAGINATION_SIZE = "paginationSize";

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryH2ServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @PostConstruct
    private void init() {
        try {
            this.paginationSize = Integer.parseInt(env.getRequiredProperty(KEY_PAGINATION_SIZE));
        } catch (NumberFormatException e) {
            LOG.error("Incorrect data in file-'/resources/app.properties'  field-'paginationSize' ", e);
        }
    }

    @Override
    public Category addCategory(Category category) {
        return categoryRepository.saveAndFlush(category);
    }

    @Override
    public boolean delete(Long id) {
        try {
            categoryRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    @Override
    public Page<Category> getByName(String name, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, paginationSize);
        return categoryRepository.findByName(name, pageable);
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public Category getById(Long id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public Category editCategory(Category category) {
        return categoryRepository.saveAndFlush(category);
    }

    @Override
    public Page<Category> getAll(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, paginationSize);
        return categoryRepository.findAll(pageable);
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }


    @Override
    public void deleteAll() {
        categoryRepository.deleteAllInBatch();
    }

    public void moveSubCategoryToSuperSuper(Category category) {
        Set<Category> subCategoryList = category.getSubCategories();
        Category superCategory = category.getSuperCategory();
        if (!subCategoryList.isEmpty()) {
            for (Category subCategory : subCategoryList) {
                subCategory.setSuperCategory(superCategory);
                editCategory(subCategory);
            }
        }
    }

    public  boolean isExist(Long id){
        return categoryRepository.existsById(id);
    }

}
