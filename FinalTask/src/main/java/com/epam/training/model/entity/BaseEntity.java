package com.epam.training.model.entity;

/**
 * Base class for describing model elements.
 * Used to create a hierarchy,
 * for writing polymorphic methods
 */
public abstract class BaseEntity {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
