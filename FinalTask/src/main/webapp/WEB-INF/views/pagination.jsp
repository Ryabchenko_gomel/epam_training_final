<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<html>

<c:if test="${not empty searchParam}">
    <c:set var="urlAddSearchParam" scope="request" value="${searchParam}"/>
</c:if>


<c:if test="${not empty page}">
    <div align="center">
        <c:choose>
            <%-- Show Prev as link if not on first page --%>
            <c:when test="${hasPrevious}">
                <c:url value="${pageUrl}${pageNumber}${urlAddSearchParam}" var="url"/>
                <a href='<c:out value="${url}" />'>Prev page</a>
            </c:when>
            <c:otherwise>
                <xmp>  Prev page</xmp>
            </c:otherwise>
        </c:choose>

        <c:forEach begin="1" end="${page.totalPages}" step="1" varStatus="tagStatus">
            <c:choose>
                <%-- In PagedListHolder page count starts from 0 --%>
                <c:when test="${(pageNumber+1) == tagStatus.index}">
                    <span>page ${tagStatus.index}</span><b> </b>
                </c:when>
                <c:otherwise>
                    <c:url value="${pageUrl}${tagStatus.index}${urlAddSearchParam}" var="url"/>
                    <a href='<c:out value="${url}"/>'>page ${tagStatus.index} </a><b> </b>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:choose>
            <%-- Show Next as link if not on last page --%>
            <c:when test="${hasNext}">
                <c:url value="${pageUrl}${pageNumber+2}${urlAddSearchParam}" var="url"/>
                <a href='<c:out value="${url}" />'>Next page</a>
            </c:when>
            <c:otherwise>
                <xmp>  Next page</xmp>
            </c:otherwise>
        </c:choose>
    </div>
</c:if>
</body>
</html>
