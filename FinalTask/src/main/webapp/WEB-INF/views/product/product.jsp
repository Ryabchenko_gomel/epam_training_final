<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Product</title>
</head>
<body>
<div align="center"><b>Product edited:</b></div>
<hr>
<br>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='product'/>
</c:import>
<hr>
<br>
<div   role="alert">
    <strong>${msg}</strong>
</div>

<div class="container">
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <spring:url value="/v1/product/${product.id}" var="productActionUrl"/>

        <form:form name="productInput" method="put" modelAttribute="productDtoForm" action="${productActionUrl}">
            <tr>
                <th>
                    Old values
                </th>
            </tr>
            <tr>
                <th width="5%">ID</th>
                <th width="15%">Name</th>
                <th width="30%">Categories</th>
                <th width="10%">Prices</th>
            </tr>
            <tr>
            </tr>
            <tr>
                <td>
                    <c:out value="${product.id}"/>
                </td>
                <td>
                    <c:out value="${product.name}"/>
                </td>
                <td>
                    <c:if test="${not empty product.categories}">
                        <c:forEach var="categorie" items="#{product.categories}">
                            <c:out value="${categorie.name}; "/>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty product.categories}">
                        Empty
                    </c:if>
                </td>
                <td>
                    <c:if test="${not empty product.prices}">
                        <c:forEach var="price" items="#{product.prices}">
                            <c:out value="${price.currency} = "/> <c:out value="${price.value}"/>
                        </c:forEach>
                    </c:if>
                </td>
            </tr>
            <tr>
                <th>
                    New values
                </th>
            </tr>
            <tr>
                <td>
                    <c:out value="${product.id}"/>
                </td>
                <td>
                    <spring:bind path="name">
                        <label class="col-sm-2 control-label">Name</label>
                        <form:input path="name" type="text" id="name" placeholder="${product.name}"/>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="categories">
                        <label class="col-sm-2 control-label">Categories</label>
                        <form:select path="categories">
                            <form:options items="${categoryName}"/>
                        </form:select>
                    </spring:bind>
                </td>

                <td>
                    <spring:bind path="priceBYN">
                        <label class="col-sm-2 control-label">confirm price in BYN</label>
                        <form:input path="priceBYN" id="priceBYN" placeholder="0"/>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="priceUAN">
                        <label class="col-sm-2 control-label">confirm price in UAN</label>
                        <form:input path="priceUAN" id="priceUAN" placeholder="0"/>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="priceUSD">
                        <label class="col-sm-2 control-label">confirm price in USD</label>
                        <form:input path="priceUSD" id="priceUSD" placeholder="0"/>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="priceEUR">
                        <label class="col-sm-2 control-label">confirm price in EUR</label>
                        <form:input path="priceEUR" id="priceEUR" placeholder="0"/>
                    </spring:bind>
                </td>

                <td>
                    <div class="form-group" align="center">
                        <div class="col-sm-offset-5 col-sm-5">
                            <button type="submit" class="btn-lg btn-primary pull-right">Edit</button>
                        </div>
                    </div>
                </td>
            </tr>
        </form:form>

    </table>
</div>
</body>
</html>
