<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Product</title>
</head>
<body>




<c:choose>
    <c:when test="${empty pricesPage}">
        <c:set var="page" scope="request" value="${productsPage}"/>
        <c:set var="pageNumber" scope="request" value="${page.pageable.pageNumber}"/>
        <c:set var="hasPrevious" scope="request" value="${page.hasPrevious()}"/>
        <c:set var="hasNext" scope="request" value="${page.hasNext()}"/>
        <c:choose>
            <c:when test="${not empty productsPage}">
                <c:set var="products" scope="request" value="${productsPage.content}"/>
            </c:when>
            <c:otherwise>
                <c:set var="products" scope="request" value="${productsList}"/>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:set var="page" scope="request" value="${pricesPage}"/>
        <c:set var="pageNumber" scope="request" value="${pageNumber}"/>
        <c:set var="hasPrevious" scope="request" value="${page.hasPrevious}"/>
        <c:set var="hasNext" scope="request" value="${page.hasNext}"/>
        <c:set var="products" scope="request" value="${productsList}"/>
    </c:otherwise>
</c:choose>

<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='products'/>
</c:import>
<br>
<hr>


<div class="container">
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <spring:url value="/v1/product/0" var="productActionUrl"/>
        <form:form id="productInput" method="post" modelAttribute="productDtoForm" action="${productActionUrl}">
            <tr>
                <th>
                    <spring:bind path="name">
                        <label class="col-sm-2 control-label">Name</label>
                        <form:input path="name" type="text" id="name" placeholder="Name"/>
                    </spring:bind>
                </th>
                <th>
                    <spring:bind path="categories">
                        <label class="col-sm-2 control-label">Categories</label>
                        <form:select path="categories" id="categories">
                            <form:options items="${categoryName}" id="categories"/>
                        </form:select>
                    </spring:bind>
                </th>
                <th>
                    <spring:bind path="priceBYN">
                        <label class="col-sm-2 control-label">confirm price in BYN</label>
                        <form:input path="priceBYN" class="form-control" id="priceBYN" placeholder="0"/>
                    </spring:bind>

                </th>
                <th>
                    <spring:bind path="priceUAN">
                        <label class="col-sm-2 control-label">confirm price in UAN</label>
                        <form:input path="priceUAN" id="priceUAN" placeholder="0"/>
                    </spring:bind>

                </th>
                <th>
                    <spring:bind path="priceUSD">
                        <label class="col-sm-2 control-label">confirm price in USD</label>
                        <form:input path="priceUSD" class="form-control" id="priceUSD" placeholder="0"/>
                    </spring:bind>

                </th>
                <th>
                    <spring:bind path="priceEUR">
                        <label class="col-sm-2 control-label">confirm price in EUR</label>
                        <form:input path="priceEUR" class="form-control" id="priceEUR" placeholder="0"/>
                    </spring:bind>
                </th>

                <th>
                    <div class="form-group" align="center">
                        <div class="col-sm-offset-5 col-sm-5">
                            <button type="button" id="productSubmit" class="btn-lg btn-primary pull-right">Add
                            </button>
                        </div>
                    </div>
                </th>
            </tr>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form:form>

    </table>
</div>

<script>
    productSubmit.onclick = function create() {
        var staff = new Object();
        staff.name = document.getElementById('name').value;
        staff.categories = getSelectedOptions(document.getElementById('categories'));
        staff.priceBYN = document.getElementById('priceBYN').value;
        staff.priceUAN = document.getElementById('priceUAN').value;
        staff.priceUSD = document.getElementById('priceUSD').value;
        staff.priceEUR = document.getElementById('priceEUR').value;
        fetch(
            "${productActionUrl}",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'x-csrf-token': "${_csrf.token}"
                },

                body: JSON.stringify(staff),
            }).then(function (response) {
            if (response.status === 200) {

                location.assign(response.url);
                alert("Product add successfully!");
                return;
            }
            if (response.status === 400) {

                location.reload();
                alert("Incorrect data entered in some field!");
            } else {
                location.reload();
                alert("Up's, some error happens!");
                alert("response.status =" + response.status);
            }
        });
    };

    function getSelectedOptions(sel) {
        var opts = [], opt;
        // loop through options in select list
        for (var i = 0, len = sel.options.length; i < len; i++) {
            opt = sel.options[i];
            // check if selected
            if (opt.selected) {
                // add to array of option elements to return from this function
                opts.push(opt.value);
            }
        }
        // return array containing references to selected option elements
        return opts;
    }
</script>


<c:if test="${not empty products}">
    <div align="center"><b>Product List:</b></div>
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <tr>
            <th width="5%">ID</th>
            <th width="15%">Name</th>
            <th width="30%">Categories</th>
            <th width="30%">Prices</th>
            <th width="20%">Edit</th>
        <tr>
            <c:forEach var="product" items="#{products}">
            <td><c:out value="${product.id}"/></td>
            <td><c:out value="${product.name}"/></td>
            <td>
                <c:if test="${not empty product.categories}">
                    <c:forEach var="categorie" items="#{product.categories}">
                        <c:out value="${categorie.name}; "/>
                    </c:forEach>
                </c:if>
                <c:if test="${empty product.categories}">
                    Empty
                </c:if>
            </td>
            <td>
                <c:if test="${not empty product.prices}">
                    <c:forEach var="price" items="#{product.prices}">
                        <c:out value="${price.currency} = "/> <c:out value="${price.value}; "/>
                    </c:forEach>
                </c:if>
                <c:if test="${empty product.prices}">
                    Empty
                </c:if>
            </td>
            <td>
                <form name="EditProduct" action="/v1/product/${product.id}" method="get">
                    <input type="submit" value="Edit this product">
                </form>
                <form:form name="DeleteProduct" method="Delete" action="/v1/product/${product.id}">
                    <input type="submit" value="Delete this product">
                </form:form>
            </td>
        </tr>
        </c:forEach>
    </table>
</c:if>
<hr>

<c:import url="../pagination.jsp"/>
</body>
</html>

