<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Product</title>
</head>
<body>

<c:set var="page" scope="request" value="${pricePage}"/>
<c:set var="hasPrevious" scope="request" value="${page.hasPrevious}"/>
<c:set var="hasNext" scope="request" value="${page.hasNext}"/>

<c:set var="prices" scope="request" value="${pricesList}"/>

<%--<c:choose>--%>
    <%--<c:when test="${not empty pricePage}">--%>
        <%--<c:set var="prices" scope="request" value="${pricePage.content}"/>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
        <%--<c:set var="prices" scope="request" value="${pricesList}"/>--%>
    <%--</c:otherwise>--%>
<%--</c:choose>--%>

<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='prices'/>
</c:import>

<br>
<hr>


<c:if test="${not empty prices}">
    <div align="center"><b>Prices List:</b></div>
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <tr>
            <th width="10%">ID</th>
            <th width="20%">Product</th>
            <th width="15%">Currency</th>
            <th width="15%">Value</th>
            <th width="20%">Edit</th>
        <tr>
            <c:forEach var="price" items="#{prices}">
            <td><c:out value="${price.id}"/></td>
            <td><c:out value="id(${price.product.id}) - ${price.product.name} "/></td>
            <td><c:out value="${price.currency}"/></td>
            <td><c:out value="${price.value}"/></td>
            <td>
                <form name="EditPrice" action="/v1/price/${price.id}" method="get">
                    <input type="submit" value="Edit this price">
                </form>
                <form:form name="DeletePrice" method="Delete" action="/v1/price/${price.id}">
                    <input type="submit" value="Delete this price">
                </form:form>
            </td>
        </tr>
        </c:forEach>
    </table>
</c:if>
<hr>

<c:import url="../pagination.jsp"/>
</body>
</html>

