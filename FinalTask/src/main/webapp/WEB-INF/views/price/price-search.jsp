<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Product</title>
</head>
<body>

<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='pricesSearch'/>
</c:import>
<hr>
<br>


<c:if test="${not empty errmsg}">
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>${errmsg}</strong>
    </div>
</c:if>


<table width="20%" align="center" border="1" width="600" bgcolor="#CEF6CE">
    <tr>
        <form name="priceSearchCurrency" method="get" action="/v1/price/search/byCurrency">
            <th>
                <label class="col-sm-2 control-label">Price currency</label>
            <th>
                <select name="currencyValue" size="4">
                    <c:forEach var="currencyItem" items="#{currency}">
                        <option><c:out value="${currencyItem}"/></option>
                    </c:forEach>
                </select>
            <th>
                <div class="form-group" align="center">
                    <div class="col-sm-offset-5 col-sm-5">
                        <button type="submit" class="btn-lg btn-primary pull-right">"Search by currency"</button>
                    </div>
                </div>
            </th>
        </form>
    </tr>

    <tr>
        <form name="priceSearchProduct" method="get" action="/v1/price/search/byProduct">
            <th>
                <label class="col-sm-2 control-label">Price product</label>
            <th>
                <select name="productValue" size="10">
                    <c:forEach var="product" items="#{productsName}">
                        <option><c:out value="${product}"/></option>
                    </c:forEach>
                </select>
            <th>
                <div class="form-group" align="center">
                    <div class="col-sm-offset-5 col-sm-5">
                        <button type="submit" class="btn-lg btn-primary pull-right">"Search by product"</button>
                    </div>
                </div>
            </th>
        </form>
    </tr>

    <tr>
        <form name="priceSearchRange" method="get" action="/v1/price/search/byRange">
            <th>
                <label class="col-sm-2 control-label">Price currency</label>
            <th>
                <select name="currencyValue" size="4">
                    <c:forEach var="currencyItem" items="#{currency}">
                        <option><c:out value="${currencyItem}"/></option>
                    </c:forEach>
                </select>
            <th>
                <input type="text" name="minRange" placeholder="12345" required>
            <th>
                <input type="text" name="maxRange" placeholder="12345" required>
            <th>
                <div class="form-group" align="center">
                    <div class="col-sm-offset-5 col-sm-5">
                        <button type="submit" class="btn-lg btn-primary pull-right">"Search by price range"</button>
                    </div>
                </div>
            </th>
        </form>
    </tr>
</table>


</body>
</html>
