<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Price</title>
</head>
<body>
<div align="center"><b>Price edited:</b></div>
<hr>
<br>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='price'/>
</c:import>
<hr>
<br>
<div role="alert">
    <strong>${msg}</strong>
</div>

<div class="container">
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <spring:url value="/v1/price/${price.id}" var="priceActionUrl"/>

        <form:form name="priceInput" method="put" modelAttribute="priceForm" action="${priceActionUrl}">
            <tr>
                <th>
                    Old values
                </th>
            </tr>
            <tr>
                <th width="5%">ID</th>
                <th width="10%">Product</th>
                <th width="10%">Currency</th>
                <th width="50%">Value</th>
            </tr>
            <tr>
                <td>
                    <c:out value="${price.id}"/>
                </td>
                <td>
                    <c:out value="id(${price.product.id}) ${price.product.name}"/>
                </td>
                <td>
                    <c:out value="${price.currency}"/>
                </td>
                <td>
                    <c:out value="${price.value}"/>
                </td>
            </tr>
            <tr>
                <th>
                    New values
                </th>
            </tr>
            <tr>
                <td>
                    <c:out value="${price.id}"/>
                </td>
                <td>
                    <c:out value="id(${price.product.id}) ${price.product.name}"/>
                </td>
                <td>
                    <c:out value="${price.currency}"/>
                </td>

                <td>
                    <spring:bind path="value">
                        <label class="col-sm-2 control-label">confirm value of the price</label>
                        <form:input path="value" type="text" id="value" placeholder="${price.value}"/>
                    </spring:bind>
                </td>
                <td>
                    <div class="form-group" align="center">
                        <div class="col-sm-offset-5 col-sm-5">
                            <button type="submit" class="btn-lg btn-primary pull-right">Edit</button>
                        </div>
                    </div>
                </td>

            </tr>
        </form:form>
    </table>
</div>
</body>
</html>
