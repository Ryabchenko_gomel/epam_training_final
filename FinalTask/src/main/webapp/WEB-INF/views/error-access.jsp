<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='error-access'/>
</c:import>
<hr>
<br>
<div align="center">
    <b4>Welcome to the store app</b4>
    <br>
    <b1> You do not have access to this page.</b1>
    <br>
    <b2>Enter a different role</b2>
</div>
<br>
<hr>

</body>
</html>

