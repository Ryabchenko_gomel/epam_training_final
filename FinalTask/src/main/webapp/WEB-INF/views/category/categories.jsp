<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Categories</title>
</head>
<body>

<c:set var="page" scope="request" value="${categoriesPage}"/>
<c:set var="pageNumber" scope="request" value="${page.pageable.pageNumber}"/>
<c:set var="hasPrevious" scope="request" value="${page.hasPrevious()}"/>
<c:set var="hasNext" scope="request" value="${page.hasNext()}"/>
<c:choose>
    <c:when test="${not empty categoriesPage}">
        <c:set var="categories" scope="request" value="${categoriesPage.content}"/>
    </c:when>
    <c:otherwise>
        <c:set var="categories" scope="request" value="${categoriesList}"/>
    </c:otherwise>
</c:choose>

<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='categories'/>
</c:import>

<hr>
<br>

<c:if test="${not empty errmsg}">
    <div role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>${errmsg}</strong>
    </div>
</c:if>


<div class="container">
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">

        <spring:url value="/v1/category/0" var="cayegoryActionUrl"/>
        <div class="container">
            <form:form id="categoryInput" method="post" modelAttribute="categoryDtoForm" action="${cayegoryActionUrl}">
                <tr>
                    <th>
                        <spring:bind path="name">
                            <label class="col-sm-10 control-label">   Name   </label>
                            <form:input path="name" type="text" id="name" placeholder="Name"/>
                        </spring:bind>
                    </th>
                    <th>
                        <spring:bind path="products">
                            <label class="col-sm-10 control-label">   Products   </label>
                            <form:select path="products" id="products">
                                <form:options items="${productsName}" id="products"/>
                            </form:select>
                        </spring:bind>
                    </th>

                    <td>
                        <spring:bind path="superCategory">
                            <label class="col-sm-10 control-label"> Super category  </label>
                            <form:select path="superCategory">
                                <form:options items="${categoryName}"/>
                            </form:select>
                        </spring:bind>
                    </td>
                    <td>
                        <label class="col-sm-10 control-label">Sub categories(change through seting
                            super category)</label>
                    </td>

                    <th>
                        <div class="form-group" align="center">
                            <div class="col-sm-offset-5 col-sm-5">
                                <button type="submit" id="categorySubmit" class="btn-lg btn-primary pull-right">Add
                                </button>
                            </div>
                        </div>
                    </th>
                </tr>
            </form:form>
        </div>
    </table>
</div>


<c:if test="${not empty categories}">
    <div align="center"><b>Categories List:</b></div>
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <tr>
            <th width="5%">ID</th>
            <th width="1%">Name</th>
            <th width="50%">Products</th>
            <th width="10%">Super Category</th>
            <th width="15%">Sub categories</th>
            <th width="10%">Edit</th>
        <tr>
            <c:forEach var="category" items="#{categories}">
            <td><c:out value="${category.id}"/></td>
            <td><c:out value="${category.name}"/></td>
            <td>
                <c:if test="${not empty category.products}">
                    <c:forEach var="product" items="#{category.products}">
                        <c:out value="${product.name};  "/>
                    </c:forEach>
                </c:if>
                <c:if test="${empty category.products}">
                    Empty
                </c:if>
            </td>
            <td>
                <c:if test="${not empty category.superCategory}">
                    <c:out value="${category.superCategory.name}; "/>
                </c:if>
                <c:if test="${empty category.superCategory}">
                    Empty
                </c:if>
            </td>
            <td>
                <c:if test="${not empty category.subCategories}">
                    <c:forEach var="categories" items="#{category.subCategories}">
                        <c:out value="${categories.name}; "/>
                    </c:forEach>
                </c:if>
                <c:if test="${empty category.subCategories}">
                    Empty
                </c:if>
            </td>
            <td>
                <form name="EditProduct" action="/v1/category/${category.id}" method="get">
                    <input type="submit" value="Edit this category">
                </form>
                <form:form name="Delete Category" method="Delete" action="/v1/category/${category.id}">
                    <input type="submit" value="Delete this category">
                </form:form>
            </td>
        </tr>
        </c:forEach>
    </table>
</c:if>
<hr>

<c:import url="../pagination.jsp"/>
</body>
</html>

