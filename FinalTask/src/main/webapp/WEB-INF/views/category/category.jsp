<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Category</title>
</head>
<body>
<div align="center"><b>Category edited:</b></div>
<hr>
<br>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='category'/>
</c:import>
<hr>
<br>
<div   role="alert">
    <strong>${msg}</strong>
</div>


<div class="container">
    <table width="80%" align="center" border="1" width="600" bgcolor="#CEF6CE">
        <spring:url value="/v1/category/${category.id}" var="categoryActionUrl"/>

        <form:form name="categoryInput" method="put" modelAttribute="categoryDtoForm" action="${categoryActionUrl}">
            <tr>
                <th>
                    Old values
                </th>
            </tr>
            <tr>
                <th width="5%">ID(unchangeable)</th>
                <th width="10%">Name</th>
                <th width="50%">Products</th>
                <th width="10%">Super Category</th>
                <th width="15%">Sub categories(unchangeable)</th>
            </tr>
            <tr>
            </tr>
            <tr>
                <td>
                    <c:out value="${category.id}"/>
                </td>
                <td>
                    <c:out value="${category.name}"/>
                </td>
                <td>
                    <c:if test="${not empty category.products}">
                        <c:forEach var="product" items="#{category.products}">
                            <c:out value="${product.name}; "/>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty category.products}">
                        Empty
                    </c:if>
                </td>
                <td>
                    <c:if test="${not empty category.superCategory}">
                        <c:out value="${category.superCategory.name}; "/>
                    </c:if>
                    <c:if test="${empty category.superCategory}">
                        Empty
                    </c:if>
                </td>
                <td>
                    <c:if test="${not empty category.subCategories}">
                        <c:forEach var="categories" items="#{category.subCategories}">
                            <c:out value="${categories.name}; "/>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty category.subCategories}">
                        Empty
                    </c:if>
                </td>
            </tr>
            <tr>
                <th>
                    New values
                </th>
            </tr>
            <tr>
                <td>
                    <c:out value="${category.id}"/>
                </td>
                <td>
                    <spring:bind path="name">
                        <label class="col-sm-2 control-label">Name</label>
                        <form:input path="name" type="text" id="name" placeholder="${category.name}"/>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="products">
                        <label class="col-sm-2 control-label">Products</label>
                        <form:select path="products">
                            <form:options items="${productsName}"/>
                        </form:select>
                    </spring:bind>
                </td>
                <td>
                    <spring:bind path="superCategory">
                        <label class="col-sm-2 control-label">Super category</label>
                        <form:select path="superCategory">
                            <form:options items="${categoryName}"/>
                        </form:select>
                    </spring:bind>
                </td>
                <td>
                    <c:if test="${not empty category.subCategories}">
                        <c:forEach var="categories" items="#{category.subCategories}">
                            <c:out value="${categories.name}; "/>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty category.subCategories}">
                        Empty
                    </c:if>
                </td>
                <td>
                    <div class="form-group" align="center">
                        <div class="col-sm-offset-5 col-sm-5">
                            <button type="submit" class="btn-lg btn-primary pull-right">Edit</button>
                        </div>
                    </div>
                </td>
            </tr>
        </form:form>
    </table>
</div>

<div>
   <b1> Category Hierarchy </b1>
    <c:forEach var="categoryName" items="#{categoryHierarchy}">
    <xmp><c:out value="${categoryName}"/></xmp>
    </c:forEach>
</div>

</body>
</html>
