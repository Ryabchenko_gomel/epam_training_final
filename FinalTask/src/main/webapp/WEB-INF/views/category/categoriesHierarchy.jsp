<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Categories hierarchy</title>
</head>
<body>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='categoriesHierarchy'/>
</c:import>
<hr>
<br>
<div>
    <b1> Category Hierarchy </b1>
    <br>
    <hr>
    <c:forEach var="categoryName" items="#{categoryHierarchy}">
        <xmp><c:out value="${categoryName}"/></xmp>
    </c:forEach>
</div>
</body>
</html>
