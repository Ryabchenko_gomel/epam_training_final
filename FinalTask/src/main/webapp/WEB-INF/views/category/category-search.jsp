<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Product</title>
</head>
<body>
<c:import url="/WEB-INF/views/header.jsp">
    <c:param name="page" value='categoriesSearch'/>
</c:import>

<hr>
<br>
<c:if test="${not empty errmsg}">
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>${errmsg}</strong>
    </div>
</c:if>


<table width="20%" align="center" border="1" width="600" bgcolor="#CEF6CE">
    <tr>
        <form name="categorySearchId" method="get" action="/v1/category/search/byId">
            <th>
                <label class="col-sm-2 control-label">Category ID</label>
            <th>
                <input type="text" name="categoryId" placeholder="12345">
            <th>
                <div class="form-group" align="center">
                    <div class="col-sm-offset-5 col-sm-5">
                        <button type="submit" class="btn-lg btn-primary pull-right">"Search by category id"</button>
                    </div>
                </div>
            </th>
        </form>
    </tr>

    <tr>
        <form name="categorySearchName" method="get" action="/v1/category/search/byName">
            <th>
                <label class="col-sm-2 control-label">Name</label>
            <th>
                <input type="text" name="name" placeholder="category name">
            <th>
                <div class="form-group" align="center">
                    <div class="col-sm-offset-5 col-sm-5">
                        <button type="submit" class="btn-lg btn-primary pull-right">"Search by category name"</button>
                    </div>
                </div>
            </th>
        </form>
    </tr>
</table>

</body>
</html>
