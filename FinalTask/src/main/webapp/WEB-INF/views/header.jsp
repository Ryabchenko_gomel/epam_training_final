<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>header</title>
</head>
<body BGCOLOR="#FDF5E6">


<table style="width: 100%;">
    <tr>
        <th width="10%">
            <c:if test="${param.page ne 'categories'}">
                <a href="/v1/category">Categories</a>
            </c:if>
        </th>
        <th width="15%">
            <c:if test="${param.page ne 'categoriesHierarchy'}">
                <a href="/v1/category/hierarchy">Categories hierarchy</a>
            </c:if>
        </th>

        <th width="15%">
            <c:if test="${param.page ne 'categoriesSearch'}">
                <a href="/v1/category/search">Search categories</a>
            </c:if>
        </th>
        <th width="10%">
            <c:if test="${param.page ne 'products'}">
                <a href="/v1/product">Products</a>
            </c:if>
        </th>

        <th width="15%">
            <c:if test="${param.page ne 'productsSearch'}">
                <a href="/v1/product/search">Search products</a>
            </c:if>
        </th>

        <th width="10%">
            <c:if test="${param.page ne 'prices'}">
                <a href="/v1/price">Prices</a>
            </c:if>
        </th>

        <th width="15%">
            <c:if test="${param.page ne 'pricesSearch'}">
                <a href="/v1/price/search">Search prices</a>
            </c:if>
        </th>

        <th width="10%">
            <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Sign Out</a></p>
        </th>
    </tr>
</table>

</body>
</html>