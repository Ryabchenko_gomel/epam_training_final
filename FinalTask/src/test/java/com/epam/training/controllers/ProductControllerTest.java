package com.epam.training.controllers;

import com.epam.training.config.DataConfig;
import com.epam.training.config.WebAppConfig;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.entity.dto.ProductDto;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Class to test ProductController and StartController
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {DataConfig.class, WebAppConfig.class})
public class ProductControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    @Autowired
    ProductH2ServiceImpl productH2Service;
    @Autowired
    CategoryH2ServiceImpl categoryH2Service;

    private final String BASE_URL = "/v1/product/";

    Long id = 1L;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        List<Product> productList = productH2Service.getAll();
        id = productList.get(productList.size() - 1).getId();
    }

    @Test
    public void givenWac__whenServletContext__thenItProvidesGreetController() {
        ServletContext servletContext = wac.getServletContext();
        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("productH2ServiceImpl"));
    }

    @Test
    public void welcomePage() throws Exception {
        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }


    @Test
    public void getProducts() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/page/{page}", 1))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsPage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.view().name("product/products"));
    }

    @Test
    public void getProductFound() throws Exception {
        Product productExpected = productH2Service.getById(id);
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", id))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("product"))
                .andExpect(MockMvcResultMatchers.model().attribute("product", productExpected))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.view().name("product/product"));
    }

    @Test
    public void getProductNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", 0))
                .andDo(print());
        resultActions.andExpect(status().is(404));
    }

    @Test
    public void getSearch() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/search"))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("currency", Currency.values()))
                .andExpect(MockMvcResultMatchers.view().name("product/product-search"));
    }

    @Test
    public void getSearchByIdFound() throws Exception {
        Product product = productH2Service.getById(id);
        List<Product> productsListExpected = new ArrayList<>();
        productsListExpected.add(product);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("productId", id.toString()))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsList"))
                .andExpect(MockMvcResultMatchers.model().attribute("productsList", productsListExpected))
                .andExpect(MockMvcResultMatchers.view().name("product/products"));
    }

    @Test
    public void getSearchByIdNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("productId", "0"))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByIdIncorrectDate() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("productId", "SOMETHING"))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in some field!"));
    }

    @Test
    public void getSearchByNameFound() throws Exception {
        String name = productH2Service.getById(id).getName();
        String url = BASE_URL + "search/byName/";
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(url)
                        .param("name", name))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsPage"))
                .andExpect(MockMvcResultMatchers.model().attribute("pageUrl", url))
                .andExpect(MockMvcResultMatchers.model().attribute("searchParam", "?name=" + name))
                .andExpect(MockMvcResultMatchers.view().name("product/products"));
    }

    @Test
    public void getSearchByNameNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byName")
                        .param("name", "productNotExist"))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByPriceFound() throws Exception {
        Set<Price> priceSet;
        Price priceS = null;
        Long localId = 1L;
        while (priceS == null) {
            priceSet = productH2Service.getById(localId).getPrices();
            priceS = priceSet.stream().findFirst().orElse(null);
            localId++;
        }
        String priceCurrency = priceS.getCurrency().name();
        String priceValue = Integer.toString(priceS.getValue());
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byPrice")
                        .param("priceValue", priceValue)
                        .param("priceCurrency", priceCurrency))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricesPage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsList"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageNumber"))
                .andExpect(MockMvcResultMatchers.model().attribute("searchParam",
                        "?priceCurrency=" + priceCurrency + "&priceValue=" + priceValue))
                .andExpect(MockMvcResultMatchers.view().name("product/products"));
    }

    @Test
    public void getSearchByPriceIncorrectData() throws Exception {
        String priceCurrency = Currency.values()[1].getName();
        String priceValue = "SOMETHING";
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byPrice")
                        .param("priceValue", priceValue)
                        .param("priceCurrency", priceCurrency))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in price value field!"));
    }

    @Test
    public void getSearchByPriceNullValue() throws Exception {
        String priceCurrency = Currency.values()[1].getName();
        String priceValue = null;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byPrice")
                        .param("priceValue", priceValue)
                        .param("priceCurrency", priceCurrency))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Price value cannot be empty"));
    }

    @Test
    public void getSearchByPriceIncorrectCurrency() throws Exception {
        String priceCurrency = "SOMETHING";
        String priceValue = "1";
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byPrice")
                        .param("priceValue", priceValue)
                        .param("priceCurrency", priceCurrency))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in currency  field!"));
    }

    @Test
    public void getSearchByPriceNotFound() throws Exception {
        String priceCurrency = Currency.values()[1].getName();
        String priceValue = "10000000";
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byPrice")
                        .param("priceValue", priceValue)
                        .param("priceCurrency", priceCurrency))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByCategoryIdFound() throws Exception {
        List<Category> categoryList = categoryH2Service.getAll();
        String categoryId = categoryList.get(categoryList.size() - 1).getId().toString();
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byCategoryId")
                        .param("categoryId", categoryId))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsPage"))
                .andExpect(MockMvcResultMatchers.model().attribute("searchParam",
                        "?categoryId=" + categoryId))
                .andExpect(MockMvcResultMatchers.view().name("product/products"));
    }

    @Test
    public void getSearchByCategoryIdIncorrectData() throws Exception {
        String categoryId = "SOMETHING";
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byCategoryId")
                        .param("categoryId", categoryId))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in Category Id field!"));
    }

    @Test
    public void getSearchByCategoryIdNotFound() throws Exception {
        List<Category> categoryList = categoryH2Service.getAll();
        String categoryId = categoryList.get(categoryList.size() - 1).getId().toString() + 1;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byCategoryId")
                        .param("categoryId", categoryId))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void addNewProductSuccess() throws Exception {
        ProductDto productDtoForm = getProductDto();
        String url = BASE_URL + "/0";
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(productDtoForm);

        ResultActions resultActions = mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print());

        resultActions.andExpect(status().is(302));

        String expected = productDtoForm.getName();
        List<Product> productList = productH2Service.getAll();
        String actual = productList.get(productList.size() - 1).getName();
        assertEquals(expected, actual);
    }

    @Test
    public void addNewProductNotValidate() throws Exception {
        ProductDto productDtoForm = getProductDto();
        productDtoForm.setPriceBYN("-1");
        String url = BASE_URL + "/0";
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(productDtoForm);

        ResultActions resultActions = mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print());

        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .view()
                        .name("product/products"));
    }

    @Test
    public void updateProductSuccess() throws Exception {
        ProductDto productDtoForm = getProductDto();
        productDtoForm.setName("new Name new Product");
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("productDtoForm", productDtoForm))
                .andDo(print());

        resultActions
                .andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Product update successfully!"));

        String expected = productDtoForm.getName();
        String actual = productH2Service.getById(id).getName();
        assertEquals(expected, actual);

    }

    @Test
    public void updateProductNotValidate() throws Exception {
        ProductDto productDtoForm = getProductDto();
        productDtoForm.setPriceBYN("-1");
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("productDtoForm", productDtoForm))
                .andDo(print());

        resultActions.andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .view()
                        .name("redirect:"
                                + BASE_URL
                                + id
                                + "?msg=Incorrect data entered in some field!"));
    }

    @Test
    public void deleteProductSuccess() throws Exception {
        int expected = productH2Service.getAll().size() - 1;
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(302));

        int actual = productH2Service.getAll().size();

        assertEquals(expected, actual);

    }

    @Test
    public void deleteProductNotFound() throws Exception {
        String url = BASE_URL + "/" + (id + 1);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(404));

    }

    private ProductDto getProductDto() {
        ProductDto productDtoForm = new ProductDto();
        productDtoForm.setName("newProduct");
        productDtoForm.setPriceBYN("100");
        productDtoForm.setPriceUAN("200");
        productDtoForm.setPriceUSD("300");
        productDtoForm.setPriceEUR("400");
        Set<String> categoryName = new HashSet<>();
        Category category = categoryH2Service.getAll().get(0);
        categoryName.add(category.getName() + " id(" + category.getId() + ") ");
        productDtoForm.setCategories(categoryName);
        return productDtoForm;
    }
}
