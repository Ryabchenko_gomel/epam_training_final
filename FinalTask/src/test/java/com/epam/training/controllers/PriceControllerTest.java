package com.epam.training.controllers;

import com.epam.training.config.DataConfig;
import com.epam.training.config.WebAppConfig;
import com.epam.training.model.entity.Currency;
import com.epam.training.model.entity.Price;
import com.epam.training.model.entity.Product;
import com.epam.training.model.restservice.impl.PriceSoapServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import com.epam.training.service.PricePage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.lang.invoke.MethodHandles;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to test PriceController
 */

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {DataConfig.class, WebAppConfig.class})
public class PriceControllerTest {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    @Autowired
    PriceSoapServiceImpl priceSoapService;
    @Autowired
    ProductH2ServiceImpl productH2Service;


    private final String BASE_URL = "/v1/price/";

    Long id = 1L;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        List<Price> priceList = priceSoapService.getAll();
        try {
            id = priceList.get(priceList.size() - 1).getId();
        } catch (ArrayIndexOutOfBoundsException e) {
            LOG.warn("Don't set data for prices. Delete data from both DB and restart SOAP and REST service.", e);
        }
    }

    @Test
    public void givenWac__whenServletContext__thenItProvidesGreetController() {
        ServletContext servletContext = wac.getServletContext();
        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("priceSoapServiceImpl"));
    }

    @Test
    public void getPrices() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(get(BASE_URL + "/page/{page}", 1))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricePage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageNumber"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricesList"))
                .andExpect(MockMvcResultMatchers.model().attribute("pageUrl", BASE_URL + "page/"))
                .andExpect(MockMvcResultMatchers.view().name("price/prices"));
    }

    @Test
    public void getPriceFound() throws Exception {
        Price priceExpected = priceSoapService.getById(id, productH2Service);
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", id))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("price", priceExpected))
                .andExpect(MockMvcResultMatchers.model().attributeExists("priceForm"))
                .andExpect(MockMvcResultMatchers.view().name("price/price"));
    }

    @Test
    public void getPriceNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", 0))
                .andDo(print());
        resultActions.andExpect(status().is(404));
    }

    @Test
    public void getSearch() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/search"))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("currency", Currency.values()))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsName"))
                .andExpect(MockMvcResultMatchers.view().name("price/price-search"));
    }

    @Test
    public void getSearchByCurrencyFound() throws Exception {
        int pageNumber = 0;
        Currency currency = Currency.values()[3];
        int expected = priceSoapService.getByCurrency(currency, pageNumber).getContent().size();
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byCurrency/{page}", pageNumber + 1)
                        .param("currencyValue", currency.getName()))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricePage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageNumber"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricesList"))
                .andExpect(MockMvcResultMatchers.view().name("price/prices"));
        int actual = ((List<Price>) resultActions.andReturn().getModelAndView().getModel().get("pricesList")).size();
        assertEquals(expected, actual);
    }

    @Test
    public void getSearchByCurrencyIncorrectDate() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byCurrency"))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in currency field!"));
    }

    @Test
    public void getSearchByProductFound() throws Exception {
        int pageNumber = 0;
        List<Product> productList = productH2Service.getAll();
        Product product = productList.get(1);
        String productName = product.getName() + " (" + product.getId() + ") ";
        int expected = priceSoapService.getByProduct(product, pageNumber).getContent().size();
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byProduct/{page}", pageNumber + 1)
                        .param("productValue", productName))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricePage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageNumber"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricesList"))
                .andExpect(MockMvcResultMatchers.view().name("price/prices"));
        int actual = ((List<Price>) resultActions.andReturn().getModelAndView().getModel().get("pricesList")).size();
        assertEquals(expected, actual);
    }

    @Test
    public void getSearchByProductIncorrectDate() throws Exception {
        String productName = null;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byProduct")
                        .param("productValue", productName))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in product field!"));
    }

    @Test
    public void getSearchByRangeFound() throws Exception {
        int pageNumber = 0;
        Currency currency = Currency.values()[3];
        int min = 1;
        int max = 50;
        PricePage pricePage = priceSoapService.getByRange(currency, min, max, pageNumber);
        int expected = pricePage.getContent().size();
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byRange/{page}", pageNumber + 1)
                        .param("currencyValue", currency.getName())
                        .param("minRange", Integer.toString(min))
                        .param("maxRange", Integer.toString(max)))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageUrl"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricePage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pageNumber"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("pricesList"))
                .andExpect(MockMvcResultMatchers.view().name("price/prices"));
        int actual = ((List<Price>) resultActions.andReturn().getModelAndView().getModel().get("pricesList")).size();
        assertEquals(expected, actual);
    }

    @Test
    public void getSearchByRangeNotFound() throws Exception {
        int pageNumber = 0;
        Currency currency = Currency.values()[3];
        int min = 9999999;
        int max = 99999999;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byRange/{page}", pageNumber + 1)
                        .param("currencyValue", currency.getName())
                        .param("minRange", Integer.toString(min))
                        .param("maxRange", Integer.toString(max)))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByRangeIncorrectDate() throws Exception {
        int pageNumber = 0;
        Currency currency = Currency.values()[3];

        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        //pageNumber + 1   cause Utils.getPageNumber(page);
                        .get(BASE_URL + "/search/byRange/{page}", pageNumber + 1)
                        .param("currencyValue", currency.getName())
                        .param("minRange", "ERROR")
                        .param("maxRange", "ERROR"))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in price field!"));
    }

    @Test
    public void updatePriceSuccess() throws Exception {
        Price newPrice = priceSoapService.getById(id, productH2Service);
        int expectedPriceValue = newPrice.getValue() + 10;
        newPrice.setValue(expectedPriceValue);
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("priceForm", newPrice))
                .andDo(print());

        resultActions
                .andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Price update successfully!"));

        int actualPriceValue = priceSoapService.getById(id, productH2Service).getValue();
        assertEquals(expectedPriceValue, actualPriceValue);
    }

    @Test
    public void updatePriceNotValidate() throws Exception {
        Price newPrice = priceSoapService.getById(id, productH2Service);
        newPrice.setValue(-5);
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("priceForm", newPrice))
                .andDo(print());

        resultActions
                .andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .view()
                        .name("redirect:/v1/price/" + id + "?msg=Incorrect data entered in some field!"));
    }

    @Test
    public void updatePriceNotFound() throws Exception {
        String url = BASE_URL + "/" + (id + 1);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(404));
    }

    @Test
    public void deletePriceSuccess() throws Exception {
        int expected = priceSoapService.getAll().size() - 1;
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(302));

        int actual = priceSoapService.getAll().size();
        assertEquals(expected, actual);
    }

    @Test
    public void deletePriceNotFound() throws Exception {
        String url = BASE_URL + "/" + (id + 1);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(404));
    }

}
