package com.epam.training.controllers;

import com.epam.training.config.DataConfig;
import com.epam.training.config.WebAppConfig;
import com.epam.training.model.entity.Category;
import com.epam.training.model.entity.Product;
import com.epam.training.model.entity.dto.CategoryDto;
import com.epam.training.model.restservice.impl.CategoryH2ServiceImpl;
import com.epam.training.model.restservice.impl.ProductH2ServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to test CategoryController
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {DataConfig.class, WebAppConfig.class})
public class CategoryControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    @Autowired
    CategoryH2ServiceImpl categoryH2Service;
    @Autowired
    ProductH2ServiceImpl productH2Service;

    private final String BASE_URL = "/v1/category/";

    Long id = 1L;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        List<Category> categoryList = categoryH2Service.getAll();
        id = categoryList.get(categoryList.size() - 1).getId();
    }

    @Test
    public void givenWac__whenServletContext__thenItProvidesGreetController() {
        ServletContext servletContext = wac.getServletContext();
        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("categoryH2ServiceImpl"));
    }

    @Test
    public void getCategories() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(get(BASE_URL + "/page/{page}", 1))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoriesPage"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attribute("pageUrl", BASE_URL + "page/"))
                .andExpect(MockMvcResultMatchers.view().name("category/categories"));
    }

    @Test
    public void getCategoryFound() throws Exception {
        Category categoryExpected = categoryH2Service.getById(id);
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", id))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("category", categoryExpected))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryHierarchy"))
                .andExpect(MockMvcResultMatchers.view().name("category/category"));
    }

    @Test
    public void getCategoryNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/{id}", 0))
                .andDo(print());
        resultActions.andExpect(status().is(404));
    }

    @Test
    public void getCategoriesHierarchy() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/hierarchy"))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryHierarchy"))
                .andExpect(MockMvcResultMatchers.view().name("category/categoriesHierarchy"));
    }

    @Test
    public void getSearch() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(get(BASE_URL + "/search"))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("category/category-search"));
    }

    @Test
    public void getSearchByIdFound() throws Exception {
        Category category = categoryH2Service.getById(id);
        List<Category> categoriesListExpected = new ArrayList<>();
        categoriesListExpected.add(category);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("categoryId", id.toString()))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsName"))
                .andExpect(MockMvcResultMatchers.model().attribute("categoriesList", categoriesListExpected))
                .andExpect(MockMvcResultMatchers.view().name("category/categories"));
    }

    @Test
    public void getSearchByIdNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("categoryId", "0"))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByIdIncorrectDate() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("categoryId", "SOMETHING"))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in some field!"));
    }

    @Test
    public void getSearchByNameFound() throws Exception {
        String name = categoryH2Service.getById(id).getName();
        String url = BASE_URL + "search/byName/";
        Category category = categoryH2Service.getById(id);
        List<Category> categoriesListExpected = new ArrayList<>();
        categoriesListExpected.add(category);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(url)
                        .param("name", name))
                .andDo(print());
        resultActions.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoryDtoForm"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("productsName"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("categoriesPage"))
                .andExpect(MockMvcResultMatchers.model().attribute("pageUrl", url))
                .andExpect(MockMvcResultMatchers.model().attribute("searchParam", "?name=" + name))
                .andExpect(MockMvcResultMatchers.view().name("category/categories"));
    }

    @Test
    public void getSearchByNameNotFound() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "search/byId")
                        .param("categoryId", "0"))
                .andDo(print());
        resultActions.andExpect(status().is(404))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Nothing found"));
    }

    @Test
    public void getSearchByNameIncorrectDate() throws Exception {
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get(BASE_URL + "/search/byId")
                        .param("categoryId", "SOMETHING"))
                .andDo(print());
        resultActions.andExpect(status().is(400))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Incorrect data entered in some field!"));
    }


    @Test
    public void addNewCategorySuccess() throws Exception {
        CategoryDto categoryDtoForm = getCategoryDto();
        String url = BASE_URL + "/0";
        ResultActions resultActions = mockMvc.perform(post(url)
                .flashAttr("categoryDtoForm", categoryDtoForm))
                .andDo(print());

        resultActions.andExpect(status().is(302));

        String expected = categoryDtoForm.getName();
        List<Category> categoryList = categoryH2Service.getAll();
        String actual = categoryList.get(categoryList.size() - 1).getName();
        assertEquals(expected, actual);
    }

    @Test
    public void addNewCategoryNotValidate() throws Exception {
        CategoryDto categoryDtoForm = getCategoryDto();
        categoryDtoForm.setName("");
        String url = BASE_URL + "/0";

        ResultActions resultActions = mockMvc.perform(post(url)
                .flashAttr("categoryDtoForm", categoryDtoForm))
                .andDo(print());

        resultActions.andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .flash()
                        .attribute("errmsg", "Incorrect data entered in some field!"))
                .andExpect(MockMvcResultMatchers
                        .view()
                        .name("redirect:/v1/category/"));
    }

    @Test
    public void updateCategorySuccess() throws Exception {
        CategoryDto categoryDtoForm = getCategoryDto();
        categoryDtoForm.setName("new Name new Category");
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("categoryDtoForm", categoryDtoForm))
                .andDo(print());

        resultActions
                .andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .model()
                        .attribute("errmsg", "Category update successfully!"));

        String expected = categoryDtoForm.getName();
        String actual = categoryH2Service.getById(id).getName();
        assertEquals(expected, actual);

    }

    @Test
    public void updateCategoryNotValidate() throws Exception {
        CategoryDto categoryDtoForm = getCategoryDto();
        categoryDtoForm.setName("");
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .put(url)
                        .flashAttr("categoryDtoForm", categoryDtoForm))
                .andDo(print());

        resultActions.andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers
                        .view()
                        .name("redirect:"
                                + BASE_URL
                                + id
                                + "?msg=Incorrect data entered in some field!"));
    }

    @Test
    public void deleteCategorySuccess() throws Exception {
        int expected = categoryH2Service.getAll().size() - 1;
        String url = BASE_URL + "/" + id;
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(302));

        int actual = categoryH2Service.getAll().size();

        assertEquals(expected, actual);


    }

    @Test
    public void deleteCategoryNotFound() throws Exception {
        String url = BASE_URL + "/" + (id + 1);
        ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(url))
                .andDo(print());

        resultActions
                .andExpect(status().is(404));
    }

    private CategoryDto getCategoryDto() {
        CategoryDto categoryDtoForm = new CategoryDto();
        categoryDtoForm.setName("newCategory");
        Set<String> productName = new HashSet<>();
        Product product = productH2Service.getAll().get(0);
        productName.add(product.getName() + " id(" + product.getId() + ") ");
        categoryDtoForm.setProducts(productName);
        Category superCategory = categoryH2Service.getAll().get(0);
        categoryDtoForm.setSuperCategory(superCategory.getName() + " id(" + superCategory.getId() + ") ");
        return categoryDtoForm;
    }

}