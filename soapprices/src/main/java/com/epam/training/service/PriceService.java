package com.epam.training.service;

import com.epam.training.model.Price;

import javax.jws.WebService;
import java.util.List;

/**
 * Price Data Access Interface
 */
@WebService
public interface PriceService {
    void addPrice(Price price);

    void addPriceParam(int price, String currency, long productId);

    boolean delete(Long id);

    void deleteByProductId(Long productId);

    void deleteAll();

    Price getById(Long id);

    PricePage getByCurrency(String currencyStr, int page, int pageSize);

    PricePage getByProduct(Long productId, int page, int pageSize);

    PricePage getByCurrencyPrice(String currencyStr,int value, int page, int pageSize);

    List<Price> getByProductList(Long productId);

    PricePage getByRange(String currencyStr, int start, int end, int page, int pageSize);

    void editPrice(long id, int priceValue, String currency, long productId);

    List<Price> getAll();

    PricePage getAllPage(int pageNumber, int pageSize);


}

