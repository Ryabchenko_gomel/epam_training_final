package com.epam.training.service;

import com.epam.training.model.Currency;
import com.epam.training.model.Price;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.lang.invoke.MethodHandles;
import java.util.List;

/**
 * Service to access Price data
 */

@WebService(endpointInterface = "com.epam.training.service.PriceService")
public class PriceH2Service extends BaseService implements PriceService {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public PriceH2Service() {
    }

    @Override
    public void addPrice(Price price) {
        openTransactionSession();
        Session session = getSession();
        session.save(price);
        closeTransactionSesstion();
        LOG.info("addPrice(Price price) = " + price);
    }

    @Override
    @WebMethod
    public void addPriceParam(
            @WebParam(name = "priceValue")
                    int price,
            @WebParam(name = "priceCurrency")
                    String currency,
            @WebParam(name = "productId")
                    long productId) {

        Price newPrice = new Price();
        newPrice.setValue(price);
        newPrice.setCurrency(Currency.valueOf(currency));
        newPrice.setProductId(productId);

        openTransactionSession();
        Session session = getSession();
        session.save(newPrice);
        closeTransactionSesstion();
        LOG.info("addPriceParam");
    }

    private void remove(Price price) {
        openTransactionSession();
        Session session = getSession();
        session.remove(price);
        closeTransactionSesstion();
        LOG.info("remove");
    }

    @Override
    public boolean delete(Long id) {
        Price price = getById(id);
        if (price == null) {
            LOG.info("delete false");
            return false;
        }
        remove(price);
        LOG.info("delete true");
        return true;

    }

    @Override
    public void deleteByProductId(Long productId) {
        for (Price price : getByProductList(productId)) {
            delete(price.getId());
            LOG.info("deleteByProductId");
        }
    }

    @Override
    public Price getById(Long id) {
        openTransactionSession();
        String sql = "SELECT * FROM PRICE WHERE ID = :id";
        Session session = getSession();
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("id", id);
        Price price = (Price) query.getSingleResult();
        // to solve the issue of FetchType.LAZY
        price.getCurrency().toString();
        closeTransactionSesstion();
        LOG.info("getById");
        return price;
    }

    @Override
    public void editPrice(long id, int priceValue, String currency, long productId) {

        Price newPrice = getById(id);
        newPrice.setCurrency(Currency.valueOf(currency));
        newPrice.setValue(priceValue);
        newPrice.setProductId(productId);

        openTransactionSession();
        Session session = getSession();
        session.update(newPrice);
        closeTransactionSesstion();
        LOG.info("editPrice");
    }

    @Override
    public List<Price> getAll() {
        openTransactionSession();
        Session session = getSession();
        // HQL (Hibernate Query Language)
        String sql = "From " + Price.class.getSimpleName();
        List<Price> prices = session.createQuery(sql).list();
        // to solve the issue of FetchType.LAZY
        prices.size();
        closeTransactionSesstion();
        LOG.info("getAll");
        return prices;
    }

    @Override
    public void deleteAll() {
        openTransactionSession();
        Session session = getSession();
        String sql = "DELETE FROM PRICE;";
        Query query = session.createNativeQuery(sql);
        query.executeUpdate();
        closeTransactionSesstion();
        LOG.info("deleteAll");
    }

    @Override
    public PricePage getAllPage(int pageNumber, int pageSize) {
        pageNumber++;
        openTransactionSession();
        Session session = getSession();

        PricePage pricePage = new PricePage();

        //how many total page exist
        String countQ = "Select count (p.id) from Price p";
        Query countQuery = session.createQuery(countQ);
        pricePage.setPageNumberCalculateTotalPage(countQuery, pageSize, pageNumber);


        String sql = "SELECT * FROM PRICE order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        pricePage.setContentFromQuery(query, pageNumber, pageSize);

        closeTransactionSesstion();
        LOG.info("getAllPage");
        return pricePage;
    }

    @Override
    public PricePage getByCurrency(String currencyStr, int pageNumber, int pageSize) {
        openTransactionSession();
        Session session = getSession();

        PricePage pricePage = new PricePage();

        //how many total page exist
        Currency currency = Currency.valueOf(currencyStr);
        String countQ = "Select count (p.id) from Price p where currency = : currency";
        Query countQuery = session.createQuery(countQ);
        countQuery.setParameter("currency", currency);
        pricePage.setPageNumberCalculateTotalPage(countQuery, pageSize, pageNumber);


        String sql = "SELECT * FROM PRICE WHERE CURRENCY = :currency order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("currency", currencyStr);
        pricePage.setContentFromQuery(query, pageNumber, pageSize);

        closeTransactionSesstion();
        LOG.info("getByCurrency");
        return pricePage;
    }

    @Override
    public PricePage getByProduct(Long productId, int pageNumber, int pageSize) {
        openTransactionSession();
        Session session = getSession();

        PricePage pricePage = new PricePage();

        //how many total page exist
        String countQ = "Select count (p.id) from Price p where productId = : id";
        Query countQuery = session.createQuery(countQ);
        countQuery.setParameter("id", productId);
        pricePage.setPageNumberCalculateTotalPage(countQuery, pageSize, pageNumber);


        String sql = "SELECT * FROM PRICE WHERE PRODUCTID = :productId order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("productId", productId);
        pricePage.setContentFromQuery(query, pageNumber, pageSize);

        closeTransactionSesstion();
        LOG.info("getByProduct");
        return pricePage;
    }

    @Override
    public PricePage getByCurrencyPrice(String currencyStr, int value, int pageNumber, int pageSize) {
        pageNumber++;
        Currency currency = Currency.valueOf(currencyStr);
        openTransactionSession();
        Session session = getSession();
        PricePage pricePage = new PricePage();

        //how many total page exist
        String countQ = "Select count (p.id) from Price p where currency = : currency and value=:priceValue";
        Query countQuery = session.createQuery(countQ);
        countQuery.setParameter("priceValue", value);
        countQuery.setParameter("currency", currency);
        pricePage.setPageNumberCalculateTotalPage(countQuery, pageSize, pageNumber);

        String sql = "SELECT * FROM PRICE WHERE CURRENCY = :currency and VALUE=:priceValue order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("priceValue", value);
        query.setParameter("currency", currencyStr);

        pricePage.setContentFromQuery(query, pageNumber, pageSize);

        closeTransactionSesstion();
        LOG.info("getByCurrencyPrice");
        return pricePage;
    }

    @Override
    public List<Price> getByProductList(Long productId) {
        openTransactionSession();
        Session session = getSession();
        String sql = "SELECT * FROM PRICE WHERE PRODUCTID = :productId order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("productId", productId);
        List<Price> prices = (List<Price>) query.list();
        // to solve the issue of FetchType.LAZY
        prices.size();
        closeTransactionSesstion();
        LOG.info("getByProductList");
        return prices;
    }

    @Override
    public PricePage getByRange(String currencyStr, int min, int max, int pageNumber, int pageSize) {

        openTransactionSession();
        Session session = getSession();
        Currency currency = Currency.valueOf(currencyStr);

        PricePage pricePage = new PricePage();

        //how many total page exist
        String countQ = "Select count (p.id) from Price p where currency = :currency and value between :minValue and :maxValue";
        Query countQuery = session.createQuery(countQ);
        countQuery.setParameter("currency", currency);
        countQuery.setParameter("minValue", min);
        countQuery.setParameter("maxValue", max);
        pricePage.setPageNumberCalculateTotalPage(countQuery, pageSize, pageNumber);


        String sql = "SELECT * FROM PRICE WHERE CURRENCY = :currency and VALUE between :minValue and :maxValue order by id";
        Query query = session.createNativeQuery(sql).addEntity(Price.class);
        query.setParameter("currency", currencyStr);
        query.setParameter("minValue", min);
        query.setParameter("maxValue", max);
        pricePage.setContentFromQuery(query, pageNumber, pageSize);

        closeTransactionSesstion();
        LOG.info("getByRange");
        return pricePage;
    }
}

