package com.epam.training.service;

import com.epam.training.model.Price;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

/**
 * PagePrice Description Class to get pagination on view
 */
public class PricePage {

    private final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    private List<Price> content = new ArrayList<>();
    private long totalPages;
    private int pageNumber;
    private int numberOfElements;
    private boolean hasNext;
    private boolean hasPrevious;

    public PricePage() {
    }

    public List<Price> getContent() {
        return content;
    }

    public void setContent(List<Price> content) {
        this.content = content;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }


    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public boolean isHasPrevious() {
        return hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious = hasPrevious;
    }

    void setPageNumberCalculateTotalPage(Query countQuery, int pageSize, int pageNumber) {
        Long countResults = (Long) countQuery.uniqueResult();
        Long totalPages = (countResults % pageSize != 0) ? ((countResults / pageSize) + 1) : (countResults / pageSize);
        this.pageNumber = pageNumber - 1;
        this.totalPages = totalPages;
        setNextPreviousField();

    }

    void setContentFromQuery(Query query, int pageNumber, int pageSize) {
        if (pageNumber > 0) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
            List<Price> priceLit = (List<Price>) query.list();
            this.setContent(priceLit);
            this.setNumberOfElements(priceLit.size());
            LOG.info("setContentFromQuery setNumberOfElements = " + numberOfElements );
        } else {
            LOG.warn("setContentFromQuery ERROR  pageNumber <= 0");
            this.setNumberOfElements(0);
        }
    }

    private void setNextPreviousField() {
        this.hasNext = (getPageNumber()+1) < totalPages;
        this.hasPrevious = getPageNumber()+1 > 1;
    }

    @Override
    public String toString() {
        return "PricePage{" +
                "content=" + getContentString() +
                ", totalPages=" + totalPages +
                ", pageNumber=" + pageNumber +
                ", numberOfElements=" + numberOfElements +
                ", hasPrevious()=" + hasPrevious +
                ", hasNext()=" + hasNext +
                '}';
    }

    private String getContentString() {
        StringBuffer result = new StringBuffer();
        result.append("\n");
        for (Price price : content) {
            result.append(price);
            result.append("\n");
        }
        return result.toString();
    }
}
