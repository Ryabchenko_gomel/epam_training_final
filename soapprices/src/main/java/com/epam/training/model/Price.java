package com.epam.training.model;

import java.util.Objects;
import javax.persistence.*;

/**
 * Price Description Class
 */
@Entity
@Table
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int value;

    @Enumerated(EnumType.STRING)
    private Currency currency;


    private Long productId;

    public Price() {
    }

    public Price(int value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int price) {
        this.value = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", price=" + value +
                ", currency=" + currency +
                ", productId=" + productId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Price)) return false;
        Price price1 = (Price) o;
        return getValue() == price1.getValue() &&
                getId().equals(price1.getId()) &&
                getCurrency() == price1.getCurrency() &&
                getProductId().equals(price1.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getValue(), getCurrency(), getProductId());
    }
}
